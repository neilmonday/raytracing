#version 450 core

#define PI 3.1415926535897932384626433832795
#define MAX_T 99999999.0

struct Lens
{
    float cylinder_radius; 
    float cylinder_z1;
    float cylinder_z2;
    float radius1;
    float radius2;
    float center_z1;
    float center_z2;
    float refractive_index;
};

//Does not change from the start of the application
layout (local_size_x = 8, local_size_y = 8, local_size_z = 1) in;

layout (location=10) uniform uint lenses_count;
layout (location=11) uniform float seed;

layout (binding=7, std140) buffer LensData
{
    Lens lenses[];
};

layout (binding=8, std140) buffer OutputData
{
    vec4 rays[];
};

uint hash( uint x ) {
    x += ( x << 10u );
    x ^= ( x >>  6u );
    x += ( x <<  3u );
    x ^= ( x >> 11u );
    x += ( x << 15u );
    return x;
}

// Compound versions of the hashing algorithm I whipped together.
uint hash( uvec2 v ) { return hash( v.x ^ hash(v.y)                         ); }
uint hash( uvec3 v ) { return hash( v.x ^ hash(v.y) ^ hash(v.z)             ); }
uint hash( uvec4 v ) { return hash( v.x ^ hash(v.y) ^ hash(v.z) ^ hash(v.w) ); }

// Construct a float with half-open range [0:1] using low 23 bits.
// All zeroes yields 0.0, all ones yields the next smallest representable value below 1.0.
float floatConstruct( uint m ) {
    const uint ieeeMantissa = 0x007FFFFFu; // binary32 mantissa bitmask
    const uint ieeeOne      = 0x3F800000u; // 1.0 in IEEE binary32

    m &= ieeeMantissa;                     // Keep only mantissa bits (fractional part)
    m |= ieeeOne;                          // Add fractional part to 1.0

    float  f = uintBitsToFloat( m );       // Range [1:2]
    //return f - 1.0;                        // Range [0:1]
    return ((f - 1.0) * 2.0) - 1.0;        // Range [-1:1]
}

// Pseudo-random value in half-open range [0:1].
float random( float x ) { return floatConstruct(hash(floatBitsToUint(x))); }
float random( vec2  v ) { return floatConstruct(hash(floatBitsToUint(v))); }
float random( vec3  v ) { return floatConstruct(hash(floatBitsToUint(v))); }
float random( vec4  v ) { return floatConstruct(hash(floatBitsToUint(v))); }

void csgIntersection( inout float t11, 
                      inout float t12, 
                      inout float t21, 
                      inout float t22, 
                      inout vec3 normal11, 
                      inout vec3 normal12, 
                      inout vec3 normal21, 
                      inout vec3 normal22)
{
    if((t12 < t21) || (t22 < t11)) //if this is true, there is no overlap
    {
        t11 = MAX_T;
        t12 = MAX_T;
        t21 = MAX_T;
        t22 = MAX_T;
        normal11 = vec3(0.0);
        normal12 = vec3(0.0);
        normal21 = vec3(0.0);
        normal22 = vec3(0.0);
    }
    else if((t21 < t11) && (t11 < t12) && (t12 < t22))
    {
        t11 = t11;
        t12 = t12;
        t21 = MAX_T;
        t22 = MAX_T;
        normal11 = normal11;
        normal12 = normal12;
        normal21 = vec3(0.0);
        normal22 = vec3(0.0);
    }
    else if((t11 < t21) && (t21 < t22) && (t22 < t12))
    {
        t11 = t21;
        t12 = t22;
        t21 = MAX_T;
        t22 = MAX_T;
        normal11 = normal21;
        normal12 = normal22;
        normal21 = vec3(0.0);
        normal22 = vec3(0.0);
    }
    else if((t11 < t21) && (t21 < t12) && (t12 < t22))
    {
        t11 = t21;
        t12 = t12;
        t21 = MAX_T;
        t22 = MAX_T;
        normal11 = normal21;
        normal12 = normal12;
        normal21 = vec3(0.0);
        normal22 = vec3(0.0);
    }
    else if((t21 < t11) && (t11 < t22) && (t22 < t12))
    {
        t11 = t11;
        t12 = t22;
        t21 = MAX_T;
        t22 = MAX_T;
        normal11 = normal11;
        normal12 = normal22;
        normal21 = vec3(0.0);
        normal22 = vec3(0.0);
    }
}

void csgDifference( inout float t11, 
                    inout float t12, 
                    inout float t21, 
                    inout float t22, 
                    inout vec3 normal11, 
                    inout vec3 normal12, 
                    inout vec3 normal21, 
                    inout vec3 normal22)
{
    if((t12 < t21) || (t22 < t11)) //if this is true, there is no overlap
    {
        t11 = t11;
        t12 = t12;
        t21 = MAX_T;
        t22 = MAX_T;
        normal11 = normal11;
        normal12 = normal12;
        normal21 = vec3(0.0);
        normal22 = vec3(0.0);
    }
    else if((t21 < t11) && (t11 < t12) && (t12 < t22))
    {
        t11 = MAX_T;
        t12 = MAX_T;
        t21 = MAX_T;
        t22 = MAX_T;
        normal11 = vec3(0.0);
        normal12 = vec3(0.0);
        normal21 = vec3(0.0);
        normal22 = vec3(0.0);
    }
    else if((t11 < t21) && (t21 < t22) && (t22 < t12))//2 intervals...
    {
        t11 = t11;
        t12 = t21;
        t21 = t22;
        t22 = t12;
        normal11 = normal11;
        normal12 = -normal21;
        normal21 = -normal22;
        normal22 = normal12;
    }
    else if((t11 < t21) && (t21 < t12) && (t12 < t22))
    {
        t11 = t11;
        t12 = t21;
        t21 = MAX_T;
        t22 = MAX_T;
        normal11 = normal11;
        normal12 = normal21;
        normal21 = vec3(0.0);
        normal22 = vec3(0.0);
    }
    else if((t21 < t11) && (t11 < t22) && (t22 < t12))
    {
        t11 = t22;
        t12 = t12;
        t21 = MAX_T;
        t22 = MAX_T;
        normal11 = -normal22;
        normal12 = normal12;
        normal21 = vec3(0.0);
        normal22 = vec3(0.0);
    }
}

void intersectCylinder(vec3 o, vec3 d, uint k, inout float t1, inout float t2, inout vec3 normal1, inout vec3 normal2)
{
    const float r = lenses[k].cylinder_radius;
    //cylinder equation (x-f.x)^2 + (y-f.y)^2 - r*r = 0 // notice z can be anything since it is on z axis.
    //parametric rays plugged into cylinder equation 
    // (o.x + td.x)^2 + (o.y + td.y)^2 - r*r = 0
    // (o.x + td.x) * (o.x + td.x) + (o.y + td.y) * (o.y + td.y) - 9 = 0 // assume r is some const 3
    // (o.x)^2 + 2(o.x*td.x) + (td.x)^2 + (o.y)^2 + 2(o.y*td.y) + (td.y)^2 - 9 = 0
    // o.x^2 + o.y^2 + 2(o.x*td.x) + 2(o.y*td.y) + (td.x)^2 + (td.y)^2 - 9 = 0

    //quadratic equation ax^2 + bx + c = 0
    // gather all of the constants for c
    const float c =
        (o.x) * (o.x) +
        (o.y) * (o.y) -
        r * r;

    const float b =
        2 * (o.x-0) * d.x +
        2 * (o.y-0) * d.y;

    const float a = d.x * d.x + 
        d.y * d.y;
        
    float determinant = b*b - 4*a*c;

    //check the cylinder ends
    float tZ1 = (lenses[k].cylinder_z1 - o.z)/d.z;
    float tZ2 = (lenses[k].cylinder_z2 - o.z)/d.z;
    float t21 = min(tZ1, tZ2);
    float t22 = max(tZ1, tZ2);
    float z_component =  (o + t22 * d).z - (o + t21 * d).z;
    vec3 normal21 = normalize(vec3(0.0, 0.0, -z_component));
    vec3 normal22 = normalize(vec3(0.0, 0.0, z_component));

    float t11 = MAX_T;
    float t12 = MAX_T;
    vec3 normal11 = vec3(0.0);
    vec3 normal12 = vec3(0.0);
    if(determinant > 0)
    {
        // quadratic formula
        t11 = (-b + sqrt(determinant))/(2 * a);
        t12 = (-b - sqrt(determinant))/(2 * a);
        float t_small = min(t11, t12);
        float t_big = max(t11, t12);
        t11 = t_small;
        t12 = t_big;

        normal11 = normalize(vec3((o + t11 * d).xy, 0.0));
        normal12 = normalize(vec3((o + t12 * d).xy, 0.0));
    }
    csgIntersection(t11, t12, t21, t22, normal11, normal12, normal21, normal22);

    if(t11 > 0.0f && t12 > 0.0f)
    {
        t1 = t11;
        t2 = t12;
        normal1 = normal11;
        normal2 = normal12;
    }
    else
    {
        t1 = MAX_T;
        t2 = MAX_T;
    }
}

void intersectLens(vec3 o, vec3 d, uint k, inout float t1, inout float t2, inout vec3 normal1, inout vec3 normal2)
{
    float cylinder_t1 = MAX_T;
    float cylinder_t2 = MAX_T;
    vec3 cylinder_normal1 = vec3(0.0);
    vec3 cylinder_normal2 = vec3(0.0);

    intersectCylinder(o, d, k, cylinder_t1, cylinder_t2, cylinder_normal1, cylinder_normal2);

    const float r1 = lenses[k].radius1;
    const float r2 = lenses[k].radius2;

    //sphere equation (x-f.x)^2 + (y-f.y)^2 + (z-f.z)^2 - r*r = 0
    //parametric rays plugged into sphere equation
    // (o.x + td.x)^2 + (o.y + td.y)^2 + (o.z + td.z)^2 - r*r = 0
    // (o.x + td.x) * (o.x + td.x) + (o.y + td.y) * (o.y + td.y) + (o.z + td.z) * (o.z + td.z) - 9 = 0
    // (o.x)^2 + 2(o.x*td.x) + (td.x)^2 + (o.y)^2 + 2(o.y*td.y) + (td.y)^2 + (o.z)^2 + 2(o.z*td.z) + (td.z)^2 - 9 = 0
    // o.x^2 + o.y^2 + o.z^2 + 2(o.x*td.x) + 2(o.y*td.y) + 2(o.z*td.z) + (td.x)^2 + (td.y)^2 + (td.z)^2 - 9 = 0

    //quadratic equation ax^2 + bx + c = 0
    // gather all of the constants for c
    const float c1 =
        (o.x) * (o.x) +
        (o.y) * (o.y) +
        (o.z-lenses[k].center_z1) * (o.z-lenses[k].center_z1) -
        r1 * r1;
    const float c2 =
        (o.x) * (o.x) +
        (o.y) * (o.y) +
        (o.z-lenses[k].center_z2) * (o.z-lenses[k].center_z2) -
        r2 * r2;

    const float b1 =
        2 * (o.x-0) * d.x +
        2 * (o.y-0) * d.y +
        2 * (o.z-lenses[k].center_z1) * d.z;
    const float b2 =
        2 * (o.x-0) * d.x +
        2 * (o.y-0) * d.y +
        2 * (o.z-lenses[k].center_z2) * d.z;

    // t * t * d.x * d.x + t * t * d.y * d.y + t * t * d.z * d.z
    // t * t (d.x * d.x + d.y * d.y + d.z * d.z)
    const float a = d.x * d.x + 
        d.y * d.y + 
        d.z * d.z;


    //see if our ray intersects with the sphere
    //(i.e. does quadratic forumla have roots)
    float determinant1 = b1*b1 - 4*a*c1;
    float determinant2 = b2*b2 - 4*a*c2;

    float t11 = MAX_T;
    float t12 = MAX_T;
    float t21 = MAX_T;
    float t22 = MAX_T;
    vec3 normal11;
    vec3 normal12;
    vec3 normal21;
    vec3 normal22;

    if (determinant1 > 0)
    {
        // quadratic formula
        t11 = (-b1 + sqrt(determinant1))/(2 * a);
        t12 = (-b1 - sqrt(determinant1))/(2 * a);
        float t_small = min(t11, t12);
        float t_big = max(t11, t12);
        t11 = t_small;
        t12 = t_big;
        //const float new_t1 = min(t11, t12);

        normal11 = normalize((o + t11 * d) - vec3(0,0,lenses[k].center_z1));
        normal12 = normalize((o + t12 * d) - vec3(0,0,lenses[k].center_z1));
    }

    if (determinant2 > 0)
    {
        // quadratic formula
        t21 = (-b2 + sqrt(determinant2))/(2 * a);
        t22 = (-b2 - sqrt(determinant2))/(2 * a);
        float t_small = min(t21, t22);
        float t_big = max(t21, t22);
        t21 = t_small;
        t22 = t_big;
        //const float new_t1 = min(t21, t22);

        normal21 = normalize((o + t21 * d) - vec3(0,0,lenses[k].center_z2));
        normal22 = normalize((o + t22 * d) - vec3(0,0,lenses[k].center_z2));
    }

    csgIntersection(t11, t12, cylinder_t1, cylinder_t2, normal11, normal12, cylinder_normal1, cylinder_normal2); //intersection for the cylinder
    csgDifference(t11, t12, t21, t22, normal11, normal12, normal21, normal22); //difference for the 2 spheres

    if(t11 > 0.0f && t12 > 0.0f)
    {
        t1 = t11;
        t2 = t12;
        normal1 = normal11;
        normal2 = normal12;
    }
    else
    {
        t1 = MAX_T;
        t2 = MAX_T;
    }

    return;
}

void main()
{
    //size of screen (in screen coordinates)
    const ivec2 size = ivec2(
        gl_NumWorkGroups.x * gl_WorkGroupSize.x,
        gl_NumWorkGroups.y * gl_WorkGroupSize.y);

    //position in screen coordinates
    const ivec2 position = ivec2(
        gl_LocalInvocationID.x + (gl_WorkGroupID.x * gl_WorkGroupSize.x),
        gl_LocalInvocationID.y + (gl_WorkGroupID.y * gl_WorkGroupSize.y)/*,
        gl_LocalInvocationID.z*/);

    const uvec2 rays_per_pixel = uvec2(8);
    const int iterations_per_pixel = 1; //make this 3 for debug

    const vec2 view_plane_size = vec2(10.0); //-1.0 to +1.0
    const float aperture_radius = 0.2;
    const float aperture_position = -0.01;
    const float film_plane_depth = 3.0;

    vec2 view_plane_increment;
    view_plane_increment.y = view_plane_size.y / float(size.x);
    view_plane_increment.x = view_plane_size.x / float(size.y);

    uint pixel_index = (position.y * size.x) + position.x;

    vec3 ray_start = vec3(vec2(vec2(position) - (vec2(size)/2.0) + vec2(0.5)) * view_plane_increment, film_plane_depth);
    vec3 ray_end = vec3(0.0);

    for(int j = 0; j<rays_per_pixel.y; j++)
    {
        for(int i = 0; i<rays_per_pixel.x; i++)
        {
            
            uint ray_index = (pixel_index * rays_per_pixel.y * rays_per_pixel.x * iterations_per_pixel) + (j * rays_per_pixel.x * iterations_per_pixel) + i * iterations_per_pixel;

            vec2 offset;
            offset.x = (random(seed * ray_index) * 2.0 - 1.0) / (rays_per_pixel.x);
            offset.y = (random(seed * ray_index + 1) * 2.0 - 1.0) / (rays_per_pixel.y);

            float x = (((float(i)/float(rays_per_pixel.x)) * 2.0 - 1.0 + (1.0/(float(rays_per_pixel.x)))) + offset.x) * aperture_radius;
            float y = (((float(j)/float(rays_per_pixel.y)) * 2.0 - 1.0 + (1.0/(float(rays_per_pixel.y)))) + offset.y) * aperture_radius;

            float r = 0;
            float theta = 0;
            //quadrant 1
            if((x == 0) && (y ==0))
            {
                r = 0.0;
                theta = 0.0;
            }
            else if((x>=0) && (y>=0))
            {
                if(abs(x) > abs(y))
                {
                    r = x;
                    theta = y/x * PI/4.0;
                }
                else
                {
                    r = y;
                    theta = (x/y * PI/-4.0) + PI/2.0;
                }
            }
            else if((x<0) && (y>=0)) //quadrant 2
            {
                if(abs(x) > abs(y))
                {
                    r = -x;
                    theta = PI - ((y/-x) * (PI/4.0));
                }
                else
                {
                    r = y;
                    theta = PI - (((-x/y) * (PI/-4.0)) + (PI/2.0));
                }
            }
            else if((x<0) && (y<0)) //quadrant 3
            {
                if(abs(x) > abs(y))
                {
                    r = -x;
                    theta = PI + ((-y/-x) * (PI/4.0));
                }
                else
                {
                    r = -y;
                    theta = PI + (((-x/-y) * (PI/-4.0)) + (PI/2.0));
                }
            }
            else if((x>=0) && (y<0)) //quadrant 4
            {
                if(abs(x) > abs(y))
                {
                    r = x;
                    theta = 0 - ((-y/x) * (PI/4.0));
                }
                else
                {
                    r = -y;
                    theta = 0 - (((x/-y) * (PI/-4.0)) + (PI/2.0));
                }
            }
            else
            {
                r = 0.0;
                theta = 0.0;
            }

            if(r == -1)
            {
                ray_end = vec3(0.0, 0.0, film_plane_depth);
            }
            else
            {
                x = r * cos(theta);
                y = r * sin(theta);

                ray_end = vec3(x, y, aperture_position/*lenses[0].center_z1 + lenses[0].radius1 - 2.0*/);    //make the "aperture" farther from the film with the - 2.0
            }

            vec3 o = ray_start;
            vec3 d = normalize(ray_end - ray_start);
            float t1 = MAX_T;
            float t2 = MAX_T;
            vec3 normal1;
            vec3 normal2;
            int k = 0;
            intersectLens(o, d, k, t1, t2, normal1, normal2);
            
            //debug1
            //rays[2 * ray_index + 0] = vec4(ray_start, 1.0);
            //rays[2 * ray_index + 1] = vec4(o+t1*d, 1.0);

            vec3 s1 = d;
            vec3 s2;
            vec3 N = normal1;
            vec3 neg_N = -normal1;
            float eta_1 = 1.0;
            float eta_2 = lenses[0].refractive_index;

            float ratio = (eta_1/eta_2);
            float c = dot(neg_N, s1);

            //https://en.wikipedia.org/wiki/Snell%27s_law#Vector_form
            s2 = ratio * s1 + ((ratio * c) - sqrt(1 - (ratio*ratio) * (1-(c*c)))) * N;

            vec3 new_o = o + t1 * d;
            vec3 new_d = normalize(s2);
            o = new_o - new_d;
            d = new_d;
            t1 = MAX_T;
            t2 = MAX_T;
            normal1 = vec3(0.0);
            normal2 = vec3(0.0);

            intersectLens(o, d, k, t1, t2, normal1, normal2);
            
            //debug2
            //rays[2 * ray_index + 2] = vec4(o+t1*d, 1.0);
            //rays[2 * ray_index + 3] = vec4(o+t2*d, 1.0);

            //HACK we purposefully swap the negative normal, since it is for the concave piece of the lens,
            //thus, it is pointing into the lens. we want it pointing outwards
            s1 = d;
            N = normal2;       
            neg_N = -normal2;
            eta_1 = lenses[0].refractive_index;
            eta_2 = 1.0;

            ratio = (eta_1/eta_2);
            c = dot(neg_N, s1);

            //https://en.wikipedia.org/wiki/Snell%27s_law#Vector_form
            s2 = ratio * s1 + ((ratio * c) - sqrt(1 - (ratio*ratio) * (1-(c*c)))) * N;
            //debug3
            rays[2 * ray_index + 0] = vec4(o+t2*d, 1.0);
            rays[2 * ray_index + 1] = vec4(o+t2*d + normalize(s2), 1.0);
        }
    }
}

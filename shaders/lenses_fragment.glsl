#version 450 core

layout (binding=1, rgba32f) uniform image2D image;

out vec4 fs_out;

void main()
{
    ivec2 position = ivec2(gl_FragCoord.xy);

    fs_out = imageLoad(image, position);
}

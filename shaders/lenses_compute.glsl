#version 450 core

#define PI 3.1415926535897932384626433832795
#define MAX_T 99999999.0

float modified_seed;

struct Camera
{
    vec4 eye;
    vec4 at;
    vec4 up;
    vec4 padding1;
    float fov;
    float aspect;
    int rows;//unused
    int columns;//unused
};

struct Sphere
{
    vec4 position;
    float radius;
	float padding1;
	float padding2;
	float padding3;
};

struct Triangle
{
    vec4 v0;
    vec4 v1;
    vec4 v2;
	vec4 n0;
	vec4 n1;
	vec4 n2;
	vec4 padding1;
	vec4 padding2;
};

struct Lens
{
    float cylinder_radius; 
    float cylinder_z1;
    float cylinder_z2;
    float radius1;
    float radius2;
    float center_z1;
    float center_z2;
    float refractive_index;
};

struct AABB
{
    vec4 min;
    vec4 max;
};

struct Ray
{
    vec3 origin;
    vec3 direction;
    float t;
};

//A beam is just a ray with additional parameters I want to carry along with the light.
struct Beam
{
    //the shape that the beam originated from. This is
    //used to skip a shape during raycast
    uint shape;
    double falloff;
    double power;            //amplify the emissiveSpectrum values.
    float distance;         //total distance of the beam from camera to light
    Ray ray;
    bool terminated;
};

Beam my_beam;

//Does not change from the start of the application
layout (local_size_x = 8, local_size_y = 8, local_size_z = 1) in;

layout (location=4) uniform uint aabbs_count;
layout (location=5) uniform uint spheres_count;
layout (location=6) uniform uint triangles_count;
layout (location=7) uniform uint lenses_count;

layout (location=8) uniform float seed;

//layout (binding=0, rgba32f) uniform image2D image;
layout (binding=1, rgba32f) uniform image2D image;

/*layout (binding=2, std140) uniform CameraData
{
    Camera camera;
};*/

layout (binding=3, std140) uniform CameraData
{
    Camera camera;
};

layout (binding=4, std140) buffer AABBData
{
    AABB aabbs[];
};

layout (binding=5, std140) buffer SphereData
{
    Sphere spheres[];
};

layout (binding=6, std140) buffer TriangleData
{
    Triangle triangles[];
};

layout (binding=7, std140) buffer LensData
{
    Lens lenses[];
};

layout (binding=8, std140) buffer OutputData
{
    vec4 rays[];
};

uint hash( uint x ) {
    x += ( x << 10u );
    x ^= ( x >>  6u );
    x += ( x <<  3u );
    x ^= ( x >> 11u );
    x += ( x << 15u );
    return x;
}

// Compound versions of the hashing algorithm I whipped together.
uint hash( uvec2 v ) { return hash( v.x ^ hash(v.y)                         ); }
uint hash( uvec3 v ) { return hash( v.x ^ hash(v.y) ^ hash(v.z)             ); }
uint hash( uvec4 v ) { return hash( v.x ^ hash(v.y) ^ hash(v.z) ^ hash(v.w) ); }

// Construct a float with half-open range [0:1] using low 23 bits.
// All zeroes yields 0.0, all ones yields the next smallest representable value below 1.0.
float floatConstruct( uint m ) {
    const uint ieeeMantissa = 0x007FFFFFu; // binary32 mantissa bitmask
    const uint ieeeOne      = 0x3F800000u; // 1.0 in IEEE binary32

    m &= ieeeMantissa;                     // Keep only mantissa bits (fractional part)
    m |= ieeeOne;                          // Add fractional part to 1.0

    float  f = uintBitsToFloat( m );       // Range [1:2]
    //return f - 1.0;                        // Range [0:1]
    return ((f - 1.0) * 2.0) - 1.0;        // Range [-1:1]
}

// Pseudo-random value in half-open range [0:1].
float random( float x ) { return floatConstruct(hash(floatBitsToUint(x))); }
float random( vec2  v ) { return floatConstruct(hash(floatBitsToUint(v))); }
float random( vec3  v ) { return floatConstruct(hash(floatBitsToUint(v))); }
float random( vec4  v ) { return floatConstruct(hash(floatBitsToUint(v))); }

void csgIntersection( inout float t11, 
                      inout float t12, 
                      inout float t21, 
                      inout float t22, 
                      inout vec3 normal11, 
                      inout vec3 normal12, 
                      inout vec3 normal21, 
                      inout vec3 normal22)
{
    if((t12 < t21) || (t22 < t11)) //if this is true, there is no overlap
    {
        t11 = MAX_T;
        t12 = MAX_T;
        t21 = MAX_T;
        t22 = MAX_T;
        normal11 = vec3(0.0);
        normal12 = vec3(0.0);
        normal21 = vec3(0.0);
        normal22 = vec3(0.0);
    }
    else if((t21 < t11) && (t11 < t12) && (t12 < t22))
    {
        t11 = t11;
        t12 = t12;
        t21 = MAX_T;
        t22 = MAX_T;
        normal11 = normal11;
        normal12 = normal12;
        normal21 = vec3(0.0);
        normal22 = vec3(0.0);
    }
    else if((t11 < t21) && (t21 < t22) && (t22 < t12))
    {
        t11 = t21;
        t12 = t22;
        t21 = MAX_T;
        t22 = MAX_T;
        normal11 = normal21;
        normal12 = normal22;
        normal21 = vec3(0.0);
        normal22 = vec3(0.0);
    }
    else if((t11 < t21) && (t21 < t12) && (t12 < t22))
    {
        t11 = t21;
        t12 = t12;
        t21 = MAX_T;
        t22 = MAX_T;
        normal11 = normal21;
        normal12 = normal12;
        normal21 = vec3(0.0);
        normal22 = vec3(0.0);
    }
    else if((t21 < t11) && (t11 < t22) && (t22 < t12))
    {
        t11 = t11;
        t12 = t22;
        t21 = MAX_T;
        t22 = MAX_T;
        normal11 = normal11;
        normal12 = normal22;
        normal21 = vec3(0.0);
        normal22 = vec3(0.0);
    }
}

void csgDifference( inout float t11, 
                    inout float t12, 
                    inout float t21, 
                    inout float t22, 
                    inout vec3 normal11, 
                    inout vec3 normal12, 
                    inout vec3 normal21, 
                    inout vec3 normal22)
{
    if((t12 < t21) || (t22 < t11)) //if this is true, there is no overlap
    {
        t11 = t11;
        t12 = t12;
        t21 = MAX_T;
        t22 = MAX_T;
        normal11 = normal11;
        normal12 = normal12;
        normal21 = vec3(0.0);
        normal22 = vec3(0.0);
    }
    else if((t21 < t11) && (t11 < t12) && (t12 < t22))
    {
        t11 = MAX_T;
        t12 = MAX_T;
        t21 = MAX_T;
        t22 = MAX_T;
        normal11 = vec3(0.0);
        normal12 = vec3(0.0);
        normal21 = vec3(0.0);
        normal22 = vec3(0.0);
    }
    else if((t11 < t21) && (t21 < t22) && (t22 < t12))//2 intervals...
    {
        t11 = t11;
        t12 = t21;
        t21 = t22;
        t22 = t12;
        normal11 = normal11;
        normal12 = -normal21;
        normal21 = -normal22;
        normal22 = normal12;
    }
    else if((t11 < t21) && (t21 < t12) && (t12 < t22))
    {
        t11 = t11;
        t12 = t21;
        t21 = MAX_T;
        t22 = MAX_T;
        normal11 = normal11;
        normal12 = normal12;
        normal21 = vec3(0.0);
        normal22 = vec3(0.0);
    }
    else if((t21 < t11) && (t11 < t22) && (t22 < t12))
    {
        t11 = t22;
        t12 = t12;
        t21 = MAX_T;
        t22 = MAX_T;
        normal11 = -normal22;
        normal12 = normal12;
        normal21 = vec3(0.0);
        normal22 = vec3(0.0);
    }
}

void intersectSphere(vec3 o, vec3 d, uint k, inout float t1, inout float t2, inout vec3 normal)
{
    const float r = spheres[k].radius;

    //sphere equation (x-f.x)^2 + (y-f.y)^2 + (z-f.z)^2 - r*r = 0
    //parametric rays plugged into sphere equation
    // (o.x + td.x)^2 + (o.y + td.y)^2 + (o.z + td.z)^2 - r*r = 0
    // (o.x + td.x) * (o.x + td.x) + (o.y + td.y) * (o.y + td.y) + (o.z + td.z) * (o.z + td.z) - 9 = 0
    // (o.x)^2 + 2(o.x*td.x) + (td.x)^2 + (o.y)^2 + 2(o.y*td.y) + (td.y)^2 + (o.z)^2 + 2(o.z*td.z) + (td.z)^2 - 9 = 0
    // o.x^2 + o.y^2 + o.z^2 + 2(o.x*td.x) + 2(o.y*td.y) + 2(o.z*td.z) + (td.x)^2 + (td.y)^2 + (td.z)^2 - 9 = 0

    //quadratic equation ax^2 + bx + c = 0
    // gather all of the constants for c
    const float c =
        (o.x-spheres[k].position.x) * (o.x-spheres[k].position.x) +
        (o.y-spheres[k].position.y) * (o.y-spheres[k].position.y) +
        (o.z-spheres[k].position.z) * (o.z-spheres[k].position.z) -
        r * r;

    const float b =
        2 * (o.x-spheres[k].position.x) * d.x +
        2 * (o.y-spheres[k].position.y) * d.y +
        2 * (o.z-spheres[k].position.z) * d.z;

    // t * t * d.x * d.x + t * t * d.y * d.y + t * t * d.z * d.z
    // t * t (d.x * d.x + d.y * d.y + d.z * d.z)
    const float a = d.x * d.x + 
        d.y * d.y + 
        d.z * d.z;

    //see if our ray intersects with the sphere
    //(i.e. does quadratic forumla have roots)
    float determinant = b*b - 4*a*c;

    if (determinant > 0)
    {
        // quadratic formula
        t1 = (-b + sqrt(determinant))/(2 * a);
        t2 = (-b - sqrt(determinant))/(2 * a);
        float t_small = min(t1, t2);
        float t_big = max(t1, t2);
        t1 = t_small;
        t2 = t_big;
        const float new_t = min(t1, t2);
        /*if ((0 < new_t) && (new_t < my_beam.ray.t))
        {
            //vec3 new_normal = normalize((o + new_t * d) - spheres[k].position.xyz);
            //reflectBeam(new_normal, new_t, i);
            //DO SOMETHING NEW!
        }*/
    }
    return;
}

void intersectTriangle(vec3 o, vec3 d, uint k, inout float t, inout vec3 normal)
{
    vec3 lambda;
    // define the edges so that they are tip-to-tail

    // this is the triangle's normal
    vec3 new_normal = cross(vec3(triangles[k].v1 - triangles[k].v0), vec3(triangles[k].v2 - triangles[k].v0));

    //Ray: P = o + t * d;
    //Plane: Ax + By + Cz + D = 0
    //A(o.x + t*d.x) + B(o.y + t*d.y) + C(o.z + t*d.z) + D = 0
    //A*t*d.x + B*t*d.y + C*t*d.z + A*o.x + B*o.y + C*o.z + D = 0
    //t * (A*d.x + B*d.y + C*d.z) + A*o.x + B*o.y + C*o.z + D = 0

    //D = -Ax - By - Cz;
    float a = new_normal.x;
    float b = new_normal.y;
    float c = new_normal.z;
    float D = -a*triangles[k].v0.x - b*triangles[k].v0.y - c*triangles[k].v0.z;
    float new_t1 = -(a*o.x + b*o.y + c*o.z + D) / 
        (a*d.x + b*d.y + c*d.z);

    // P is where the ray intersects with the plane

    vec3 C;
    vec3 P = vec3(o) + new_t1 * vec3(d);
    
    {
        C = P - vec3(triangles[k].v0);
        lambda.x = dot(new_normal, cross(vec3(triangles[k].v1 - triangles[k].v0), C));
    }
    {
        C = P - vec3(triangles[k].v1);
        lambda.y = dot(new_normal, cross(vec3(triangles[k].v2 - triangles[k].v1), C));
    }
    {
        C = P - vec3(triangles[k].v2);
        lambda.z = dot(new_normal, cross(vec3(triangles[k].v0 - triangles[k].v2), C));
    }

    // if point P is outside of the triangle, then go onto the next triangle
    if (((lambda.x > 0) && (lambda.y > 0) && (lambda.z > 0)) &&
        (0 < new_t1) &&
        (new_t1 < t))
    {
        t = new_t1;
        normal = new_normal;
        float denominator = dot(new_normal, new_normal);

        //lambda /= denominator;
    }

    return;
}

void intersectAABB(vec3 o, vec3 d, uint k, inout float t1, inout float t2, inout vec3 normal)
{
    float tX1 = MAX_T;
    float tX2 = MAX_T;
    float tY1 = MAX_T;
    float tY2 = MAX_T;
    float tZ1 = MAX_T;
    float tZ2 = MAX_T;

    tX1 = (aabbs[k].min.x - o.x)/d.x;
    tX2 = (aabbs[k].max.x - o.x)/d.x;
    tY1 = (aabbs[k].min.y - o.y)/d.y;
    tY2 = (aabbs[k].max.y - o.y)/d.y;
    tZ1 = (aabbs[k].min.z - o.z)/d.z;
    tZ2 = (aabbs[k].max.z - o.z)/d.z;

    float tXmax = max(tX1, tX2);
    float tXmin = min(tX1, tX2);
    float tYmax = max(tY1, tY2);
    float tYmin = min(tY1, tY2);
    float tZmax = max(tZ1, tZ2);
    float tZmin = min(tZ1, tZ2);

    /*if( (tXmax == MAX_T) || (tXmin == MAX_T) || 
        (tYmax == MAX_T) || (tYmin == MAX_T) || 
        (tZmax == MAX_T) || (tZmin == MAX_T))
    {
        t1 = MAX_T;
        t2 = MAX_T;
    }
    else
    {*/
        t1 = max(max(tXmin, tYmin), tZmin);
        t2 = min(min(tXmax, tYmax), tZmax);
            if(t1 > t2)
        {
            t1 = MAX_T;
            t2 = MAX_T;
        }
    //}
}

void intersectCylinder(vec3 o, vec3 d, uint k, inout float t1, inout float t2, inout vec3 normal)
{
    const float r = lenses[k].cylinder_radius;
    //cylinder equation (x-f.x)^2 + (y-f.y)^2 - r*r = 0 // notice z can be anything since it is on z axis.
    //parametric rays plugged into cylinder equation 
    // (o.x + td.x)^2 + (o.y + td.y)^2 - r*r = 0
    // (o.x + td.x) * (o.x + td.x) + (o.y + td.y) * (o.y + td.y) - 9 = 0 // assume r is some const 3
    // (o.x)^2 + 2(o.x*td.x) + (td.x)^2 + (o.y)^2 + 2(o.y*td.y) + (td.y)^2 - 9 = 0
    // o.x^2 + o.y^2 + 2(o.x*td.x) + 2(o.y*td.y) + (td.x)^2 + (td.y)^2 - 9 = 0

    //quadratic equation ax^2 + bx + c = 0
    // gather all of the constants for c
    const float c =
        (o.x) * (o.x) +
        (o.y) * (o.y) -
        r * r;

    const float b =
        2 * (o.x-0) * d.x +
        2 * (o.y-0) * d.y;

    const float a = d.x * d.x + 
        d.y * d.y;
        
    float determinant = b*b - 4*a*c;

    //check the cylinder ends
    float tZ1 = (lenses[k].cylinder_z1 - o.z)/d.z;
    float tZ2 = (lenses[k].cylinder_z2 - o.z)/d.z;
    float t21 = min(tZ1, tZ2);
    float t22 = max(tZ1, tZ2);
    float z_component =  (o + t22 * d).z - (o + t21 * d).z;
    vec3 normal21 = normalize(vec3(0.0, 0.0, -z_component));
    vec3 normal22 = normalize(vec3(0.0, 0.0, z_component));

    float t11 = MAX_T;
    float t12 = MAX_T;
    vec3 normal11 = vec3(0.0);
    vec3 normal12 = vec3(0.0);
    if(determinant > 0)
    {
        // quadratic formula
        t11 = (-b + sqrt(determinant))/(2 * a);
        t12 = (-b - sqrt(determinant))/(2 * a);
        float t_small = min(t11, t12);
        float t_big = max(t11, t12);
        t11 = t_small;
        t12 = t_big;

        normal11 = normalize(vec3((o + t11 * d).xy, 0.0));
        normal12 = normalize(vec3((o + t12 * d).xy, 0.0));
    }
    csgIntersection(t11, t12, t21, t22, normal11, normal12, normal21, normal22);

    if(t11 > 0.0f && t12 > 0.0f)
    {
        t1 = t11;
        t2 = t12;
        normal = normal11;
    }
    else
    {
        t1 = MAX_T;
        t2 = MAX_T;
    }
}

void intersectLens(vec3 o, vec3 d, uint k, inout float t1, inout float t2, inout vec3 normal)
{
    float cylinder_t1 = MAX_T;
    float cylinder_t2 = MAX_T;
    vec3 cylinder_normal = vec3(0.0);

    intersectCylinder(o, d, k, cylinder_t1, cylinder_t2, cylinder_normal);

    const float r1 = lenses[k].radius1;
    const float r2 = lenses[k].radius2;

    //sphere equation (x-f.x)^2 + (y-f.y)^2 + (z-f.z)^2 - r*r = 0
    //parametric rays plugged into sphere equation
    // (o.x + td.x)^2 + (o.y + td.y)^2 + (o.z + td.z)^2 - r*r = 0
    // (o.x + td.x) * (o.x + td.x) + (o.y + td.y) * (o.y + td.y) + (o.z + td.z) * (o.z + td.z) - 9 = 0
    // (o.x)^2 + 2(o.x*td.x) + (td.x)^2 + (o.y)^2 + 2(o.y*td.y) + (td.y)^2 + (o.z)^2 + 2(o.z*td.z) + (td.z)^2 - 9 = 0
    // o.x^2 + o.y^2 + o.z^2 + 2(o.x*td.x) + 2(o.y*td.y) + 2(o.z*td.z) + (td.x)^2 + (td.y)^2 + (td.z)^2 - 9 = 0

    //quadratic equation ax^2 + bx + c = 0
    // gather all of the constants for c
    const float c1 =
        (o.x) * (o.x) +
        (o.y) * (o.y) +
        (o.z-lenses[k].center_z1) * (o.z-lenses[k].center_z1) -
        r1 * r1;
    const float c2 =
        (o.x) * (o.x) +
        (o.y) * (o.y) +
        (o.z-lenses[k].center_z2) * (o.z-lenses[k].center_z2) -
        r2 * r2;

    const float b1 =
        2 * (o.x-0) * d.x +
        2 * (o.y-0) * d.y +
        2 * (o.z-lenses[k].center_z1) * d.z;
    const float b2 =
        2 * (o.x-0) * d.x +
        2 * (o.y-0) * d.y +
        2 * (o.z-lenses[k].center_z2) * d.z;

    // t * t * d.x * d.x + t * t * d.y * d.y + t * t * d.z * d.z
    // t * t (d.x * d.x + d.y * d.y + d.z * d.z)
    const float a = d.x * d.x + 
        d.y * d.y + 
        d.z * d.z;


    //see if our ray intersects with the sphere
    //(i.e. does quadratic forumla have roots)
    float determinant1 = b1*b1 - 4*a*c1;
    float determinant2 = b2*b2 - 4*a*c2;

    float t11 = MAX_T;
    float t12 = MAX_T;
    float t21 = MAX_T;
    float t22 = MAX_T;
    vec3 normal11;
    vec3 normal12;
    vec3 normal21;
    vec3 normal22;

    if (determinant1 > 0)
    {
        // quadratic formula
        t11 = (-b1 + sqrt(determinant1))/(2 * a);
        t12 = (-b1 - sqrt(determinant1))/(2 * a);
        float t_small = min(t11, t12);
        float t_big = max(t11, t12);
        t11 = t_small;
        t12 = t_big;
        //const float new_t1 = min(t11, t12);

        normal11 = normalize((o + t11 * d) - vec3(0,0,lenses[k].center_z1));
        normal12 = normalize((o + t12 * d) - vec3(0,0,lenses[k].center_z1));
    }

    if (determinant2 > 0)
    {
        // quadratic formula
        t21 = (-b2 + sqrt(determinant2))/(2 * a);
        t22 = (-b2 - sqrt(determinant2))/(2 * a);
        float t_small = min(t21, t22);
        float t_big = max(t21, t22);
        t21 = t_small;
        t22 = t_big;
        //const float new_t1 = min(t21, t22);

        normal21 = normalize((o + t21 * d) - vec3(0,0,lenses[k].center_z2));
        normal22 = normalize((o + t22 * d) - vec3(0,0,lenses[k].center_z2));
    }
    vec3 dummy22;//vec3(0.0)
    csgIntersection(t11, t12, cylinder_t1, cylinder_t2, normal11, normal12, cylinder_normal, dummy22); //intersection for the cylinder
    csgDifference(t11, t12, t21, t22, normal11, normal12, normal21, normal22); //difference for the 2 spheres

    if(t11 > 0.0f && t12 > 0.0f)
    {
        t1 = t11;
        t2 = t12;
        normal = normal11;
    }
    else
    {
        t1 = MAX_T;
        t2 = MAX_T;
    }

    return;
}

void main()
{
    //size of screen (in screen coordinates)
    const ivec2 size = ivec2(
        gl_NumWorkGroups.x * gl_WorkGroupSize.x,
        gl_NumWorkGroups.y * gl_WorkGroupSize.y);

    //position in screen coordinates
    const ivec2 position = ivec2(
        gl_LocalInvocationID.x + (gl_WorkGroupID.x * gl_WorkGroupSize.x),
        gl_LocalInvocationID.y + (gl_WorkGroupID.y * gl_WorkGroupSize.y)/*,
        gl_LocalInvocationID.z*/);

    vec2 view_plane_size;
    view_plane_size.y = length(camera.at.xyz - camera.eye.xyz) * tan((camera.fov / 2.0f) * PI / 180.0f);
    view_plane_size.x = view_plane_size.y * camera.aspect;

    vec2 view_plane_increment;
    view_plane_increment.y = view_plane_size.y / (camera.rows / 2);
    view_plane_increment.x = view_plane_size.x / (camera.columns / 2);

    //Right Handed
    vec3 w = normalize(camera.at.xyz - camera.eye.xyz);
    vec3 u = normalize(cross(w, camera.up.xyz)); //RH
    vec3 v = cross(u, w); //RH no need to normalize since u and w are orthogonal unit vectors
    //int iteration = 0;
    //float total_falloff = 0.0;

    vec3 color;
    vec2 offset = vec2(0.5);
    vec2 pixel_center = vec2(vec2(position) - (vec2(size) / 2.0f)) + offset;
    vec3 u_prime = u * pixel_center.x * view_plane_increment.x;
    vec3 v_prime = v * pixel_center.y * view_plane_increment.y;
    vec3 d = normalize(u_prime + v_prime + (camera.at.xyz - camera.eye.xyz));
    vec3 o = camera.eye.xyz;
    vec3 normal;
    float t1 = MAX_T;
    float t2 = MAX_T;
    int closest_k = -1;
    vec3 sample_color = vec3(0.0, 0.0, 0.0);

    //int previous_k = -1;
    //modified_seed += iteration * size.y * size.x;

    /*for (int k = 0; k < aabbs_count; k++)
    {
        float new_t1 = MAX_T;
        float new_t2 = MAX_T;
        vec3 new_normal;
        intersectAABB(o, d, k, new_t1, new_t2, new_normal);
        
        //I know t1 is the min at this point.
        if (new_t1 < MAX_T)
        {
            t1 = new_t1;
            t2 = new_t2;
            closest_k = k;
        }
    }

    for (int k = 0; k < spheres_count; k++)
    {
        float new_t1 = MAX_T;
        float new_t2 = MAX_T;
        vec3 new_normal;
        intersectSphere(o, d, k, new_t1, new_t2, new_normal);
        
        if (new_t1 < t1)
        {
            t1 = new_t1;
            t2 = new_t2;
            closest_k = k;
        }
    }

    for (int k = 0; k < triangles_count; k++)
    {
        float new_t1 = MAX_T;
        vec3 new_normal;
        intersectTriangle(o, d, k, new_t1, new_normal);
        
        if (new_t1 < t1)
        {
            t1 = new_t1;
            normal = new_normal;
            closest_k = k;
        }
    }*/

        
    for (int k = 0; k < lenses_count; k++)
    {
        float new_t1 = MAX_T;
        float new_t2 = MAX_T;
        vec3 new_normal;
        intersectLens(o, d, k, new_t1, new_t2, new_normal);
        //intersectCylinder(o, d, k, new_t1, new_t2, new_normal);
        if (new_t1 < t1)
        {
            t1 = new_t1;
            t2 = new_t2;
            normal = new_normal;
            closest_k = k;
        }
    }

    color = vec3(dot(-d, normal));

    //debug
    //color = vec3(vec2(position)/vec2(size), 0.5);

    imageStore(image, position, vec4(color,1.0));
    /*vec4 pixel = imageLoad(image, position);
    pixel.rgb *= pixel.a;
    pixel.a += 1.0;
    pixel.rgb += color;
    pixel.rgb /= pixel.a;
    imageStore(image, position, pixel);*/

    /*vec4 pixel = imageLoad(image, position);
    pixel.rgb *= 5.0;
    pixel.a += 1.0;
    pixel.rgb += color;
    pixel.rgb /= 6.0;
    imageStore(image, position, pixel);*/
}

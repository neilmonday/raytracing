#ifndef SCENE_H
#define SCENE_H

#include "utility.h"

#include "aabb.h"
#include "camera.h"
#include "sphere.h"
#include "triangle.h"
#include "lens.h"

typedef struct
{
    Camera main_camera;
    Camera lenses_camera;

    AABB* aabbs;
    Sphere* spheres;
    Triangle *triangles;
    Lens *lenses;

    unsigned int aabbs_count;
    unsigned int spheres_count;
    unsigned int triangles_count;
    unsigned int lenses_count;
} Scene;

void init_scene(Scene *scene);


#endif //SCENE_H
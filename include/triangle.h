#ifndef TRIANGLE_H
#define TRIANGLE_H

#include "utility.h"

typedef struct
{
    vec4 v0;
    vec4 v1;
    vec4 v2;
    vec4 n0;
    vec4 n1;
    vec4 n2;
    vec4 padding1;
    vec4 padding2;
} Triangle;

void init_triangle(Triangle *triangle);

#endif //TRIANGLE_H
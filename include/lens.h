#ifndef LENS_H
#define LENS_H

#include "utility.h"

typedef struct
{
    float cylinder_radius; 
    float cylinder_z1;
    float cylinder_z2;
    float radius1;
    float radius2;
    float center_z1;
    float center_z2;
    float refractive_index;
} Lens;

void init_lens(Lens *lens);

#endif //LENS_H
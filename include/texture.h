#ifndef TEXTURE_H
#define TEXTURE_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

typedef struct
{
    GLubyte* imageData;
    GLenum internalFormat;
    GLenum format;
    GLuint bpp;
    GLuint width;
    GLuint height;
    GLuint depth;
    GLuint texID;
    GLuint type;
} Texture;

typedef struct
{
    GLubyte Header[12];
} TGAHeader;

typedef struct
{
    GLubyte header[6];
    GLuint bytesPerPixel;
    GLuint imageSize;
    GLuint type;
    GLuint Height;
    GLuint Width;
    GLuint Bpp;
} TGA;

Texture CreateImage(GLuint width, GLuint height);
Texture LoadSkybox();
Texture LoadTable();
GLuint LoadTGA(Texture * texture, char * filename);
GLuint LoadUncompressedTGA(Texture * texture, char * filename, FILE * fTGA);
void UpdateImage(Texture texture);

#endif //TEXTURE_H

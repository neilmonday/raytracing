#ifndef OUTPUT_H
#define OUTPUT_H

#include <stdio.h>
#include <stdlib.h>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "camera.h"

void begin_output(FILE* file);
void end_output(FILE* file);
void output_axes_raybundle(FILE* file);
void output_raybundle(FILE* file, GLuint count, GLfloat* data);
void output_camera_raybundle(FILE* file, Camera camera, vec3 u, vec3 v, vec2 view_plane_increment);

#endif // !OUTPUT_H

#ifndef UTILITY_H
#define UTILITY_H

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <windows.h>
#include <tchar.h>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#ifdef _MSC_VER
#define SOFT_ASSERT(x) if(!x){ printf("Assertion! Line: %d File: %s \n", __LINE__, __FILE__); __debugbreak();}
#endif


// Vectors
typedef struct
{
    float x, y;
} vec2;

typedef struct
{
    float x, y, z;
} vec3;

typedef struct
{
    float x, y, z, w;
} vec4;

// Double Vectors
typedef struct
{
    double x, y;
} dvec2;

typedef struct
{
    double x, y, z;
} dvec3;

typedef struct
{
    double x, y, z, w;
} dvec4;

// Matrices 
typedef struct
{
    vec2 x, y;
} mat2;

typedef struct
{
    vec3 x, y, z;
} mat3;

typedef struct
{
    vec4 x, y, z, w;
} mat4;

// Double Matrices
typedef struct
{
    dvec2 x, y;
} dmat2;

typedef struct
{
    dvec3 x, y, z;
} dmat3;

typedef struct
{
    dvec4 x, y, z, w;
} dmat4;



mat4 multiply_mat4(mat4*const A, mat4*const B);

mat3 multiply_mat3(mat3*const A, mat3*const B);

vec4 multiply_mat4_vec4(mat4*const A, vec4*const B);

vec4 multiply_mat3_vec4(mat3*const A, vec4*const B);

dmat4 multiply_dmat4(dmat4*const A, dmat4*const B);

int identity(mat4* identity);

mat4 rotate(vec3 rotation);

int translate(mat4* translation, float x, float y, float z);

float length(vec3 vector);

vec3 add(vec3 A, vec3 B);

vec4 add_vec4(vec4 A, vec4 B);

vec3 subtract(vec3 A, vec3 B);

vec4 subtract_vec4(vec4 A, vec4 B);

vec3 multiply(vec3 A, float B);

vec3 divide(vec3 A, float B);

vec3 normalize(vec3 A);

vec3 cross(vec3 A, vec3 B);

void calculate_translation(GLFWwindow* window, const vec3 rotation, vec3* position);

HANDLE init_watch_folder();

void watch_folder(HANDLE dwChangeHandle, void(*rebuild)(GLuint*, GLuint*), GLuint* compute_program, GLuint* locations);
#endif //UTILITY_H

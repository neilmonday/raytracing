#ifndef CAMERA_H
#define CAMERA_H

#include "utility.h"

/*
    "camera": {
        "Eye": [1.6450000256299973, -0.2999999523162842, 8.336749156440431],
        "At": [1.6450000256299973, -0.2999999523162842, 0.7149999737739563],
        "Up": [0, 1, 0],
        "FOV": 90,
        "aspect": 1,
        "rows": 128,
        "columns": 128
    },
*/
typedef struct
{
    vec4 eye;
    vec4 at;
    vec4 up;
    vec4 padding1;
    float fov;
    float aspect;
    int rows;//unused
    int columns;//unused
} Camera;

void init_camera(Camera *camera);

#endif //CAMERA_H
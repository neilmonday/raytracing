#ifndef SPHERE_H
#define SPHERE_H

#include "utility.h"

typedef struct
{
    vec4 position;
    float radius;
    float padding1;
    float padding2;
    float padding3;
} Sphere;

void init_sphere(Sphere *sphere);

#endif //SPHERE_H
#ifndef AABB_H
#define AABB_H

#include "utility.h"

typedef struct
{
    vec4 min;
    vec4 max;
} AABB;

void init_aabb(AABB *aabb);

#endif //AABB_H
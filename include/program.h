#ifndef PROGRAM_H
#define PROGRAM_H

#include <stdio.h>
#include <stdlib.h>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "utility.h"

enum {
    RAY_GENERATION_LENSES_COUNT_LOCATION = 0,
    RAY_GENERATION_SEED_LOCATION,
    RAY_GENERATION_UNIFORM_LOCATIONS_COUNT
} RayGenerationUniformLocations;

enum {
    COMPUTE_SPHERES_COUNT_LOCATION = 0,
    COMPUTE_TRIANGLES_COUNT_LOCATION,
    COMPUTE_LENSES_COUNT_LOCATION,
    COMPUTE_AABBS_COUNT_LOCATION,
    COMPUTE_SEED_LOCATION,
    COMPUTE_ROTATION_LOCATION,
    COMPUTE_UNIFORM_LOCATIONS_COUNT
} ComputeUniformLocations;

enum {
    GRAPHICS_IMAGE_LOCATION = 0,
    GRAPHICS_UNIFORM_LOCATIONS_COUNT
} GraphicsUniformLocations;

enum {
    MAIN_CAMERA_BUFFER = 0,
    LENSES_CAMERA_BUFFER,
    SPHERES_BUFFER,
    TRIANGLES_BUFFER,
    AABBS_BUFFER,
    LENSES_BUFFER,
    OUTPUT_BUFFER,
    BUFFERS_COUNT
} Buffers;

// returns an error code 
GLuint load_and_compile_shader(GLuint* shader, GLint type, char* filename);

GLuint link_program(GLuint program,
    GLuint vertex_shader, 
    GLuint tess_ctrl_shader, 
    GLuint tess_eval_shader, 
    GLuint geometry_shader, 
    GLuint fragment_shader, 
    GLuint compute_shader);

void build_primary_ray_program(GLuint* program, GLuint* locations, char* file);
void build_graphics_program(GLuint* program, GLuint* locations, char* vertex_file, char* fragment_file);
void build_compute_program(GLuint* program, GLuint* locations, char* file);

#endif // !PROGRAM_H

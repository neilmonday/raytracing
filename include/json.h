#ifndef JSON_H
#define JSON_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <jsmn.h>

#include "scene.h"
#include "utility.h"

void parse_scene(char* filename, Scene* scene);

#endif // !JSON_H

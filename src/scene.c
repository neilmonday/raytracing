#include "scene.h"

void init_scene(Scene *scene)
{
    init_camera(&(scene->main_camera));
    init_camera(&(scene->lenses_camera));

    scene->aabbs = NULL;
    scene->spheres = NULL;
    scene->triangles = NULL;
    scene->lenses = NULL;

    scene->aabbs_count = 0;
    scene->spheres_count = 0;
    scene->triangles_count = 0;
    scene->lenses_count = 0;
}
#include "camera.h"

void init_camera(Camera *camera)
{
    camera->eye.x = 0.0f;
    camera->eye.y = 0.0f;
    camera->eye.z = 0.0f;
    camera->eye.w = 1.0f;
    camera->at.x = 0.0f;
    camera->at.y = 0.0f;
    camera->at.z = 0.0f;
    camera->at.w = 1.0f;
    camera->up.x = 0.0f;
    camera->up.y = 0.0f;
    camera->up.z = 0.0f;
    camera->up.w = 1.0f;
    
    camera->fov = 90.0f;
    camera->aspect = 1.0f;
    camera->rows = 32;
    camera->columns = 32;
}
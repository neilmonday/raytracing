#include "program.h"

// returns an error code 
GLuint load_and_compile_shader(GLuint* shader, GLint type, char* filename)
{
    long lSize;
    char * buffer;
    size_t result;

    FILE * pFile = fopen(filename, "rb");

    if (pFile == NULL) { fputs("File error", stderr); exit(1); }

    // obtain file size:
    fseek(pFile, 0, SEEK_END);
    lSize = ftell(pFile);
    rewind(pFile);

    if (lSize == 0)
    {
        fclose(pFile);
        return 1;
    }

    // allocate memory to contain the whole file:
    buffer = (char*)malloc(sizeof(char)*lSize + 1);
    buffer[lSize] = '\0';
    if (buffer == NULL) { fputs("Memory error", stderr); exit(2); }

    // copy the file into the buffer:
    result = fread(buffer, 1, lSize, pFile);
    if (result != lSize) { fputs("Reading error", stderr); exit(3); }
    /* the whole file is now loaded in the memory buffer. */

    // terminate
    fclose(pFile);
    buffer[lSize] = '\0';

    GLint isCompiled = 0;
    *shader = glCreateShader(type);
    glShaderSource(*shader, 1, &buffer, NULL);
    glCompileShader(*shader);
    glGetShaderiv(*shader, GL_COMPILE_STATUS, &isCompiled);
    if (isCompiled == GL_FALSE)
    {
        GLint maxLength = 0;
        glGetShaderiv(*shader, GL_INFO_LOG_LENGTH, &maxLength);

        //The maxLength includes the NULL character
        GLchar* infoLog = (GLchar*)malloc(maxLength * sizeof(GLchar));
        glGetShaderInfoLog(*shader, maxLength, &maxLength, &infoLog[0]);

        //We don't need the shader anymore.
        glDeleteShader(*shader);

        //Use the infoLog as you see fit.
        printf("%s", infoLog);

        SOFT_ASSERT(infoLog && 0);
        //In this simple program, we'll just leave
        return 1;
    }
    return 0;
}

GLuint link_program(GLuint program, GLuint vertex_shader, GLuint tess_ctrl_shader, GLuint tess_eval_shader, GLuint geometry_shader, GLuint fragment_shader, GLuint compute_shader)
{
    // Create program, attach shaders to it, and link it 
    if (vertex_shader) glAttachShader(program, vertex_shader);
    if (tess_ctrl_shader) glAttachShader(program, tess_ctrl_shader);
    if (tess_eval_shader) glAttachShader(program, tess_eval_shader);
    if (geometry_shader) glAttachShader(program, geometry_shader);
    if (fragment_shader) glAttachShader(program, fragment_shader);
    if (compute_shader) glAttachShader(program, compute_shader);
    glLinkProgram(program);
    GLint isLinked = 0;
    glGetProgramiv(program, GL_LINK_STATUS, &isLinked);
    if (isLinked == GL_FALSE)
    {
        GLint maxLength = 0;
        glGetProgramiv(program, GL_INFO_LOG_LENGTH, &maxLength);

        //The maxLength includes the NULL character
        GLchar* infoLog = (GLchar*)malloc(maxLength * sizeof(GLchar));
        glGetProgramInfoLog(program, maxLength, &maxLength, &infoLog[0]);

        //The program is useless now. So delete it.
        glDeleteProgram(program);

        printf("%s", infoLog);

        SOFT_ASSERT(infoLog && 0);
        //Exit with failure.
        return 1;
    }
    return 0;
}

void build_primary_ray_program(GLuint* program, GLuint* locations, char* file)
{
    GLuint err = glGetError(); SOFT_ASSERT(!err);
    GLuint compute_shader = 0;
    load_and_compile_shader(&compute_shader, GL_COMPUTE_SHADER, file);
    err = glGetError(); SOFT_ASSERT(!err);

    GLuint new_program = glCreateProgram();
    err = glGetError(); SOFT_ASSERT(!err);
    link_program(new_program, 0, 0, 0, 0, 0, compute_shader);
    err = glGetError(); SOFT_ASSERT(!err);
    if (compute_shader) glDeleteShader(compute_shader);
    err = glGetError(); SOFT_ASSERT(!err);
    locations[RAY_GENERATION_LENSES_COUNT_LOCATION] = glGetUniformLocation(new_program, "lenses_count");
    err = glGetError(); SOFT_ASSERT(!err);
    locations[RAY_GENERATION_SEED_LOCATION] = glGetUniformLocation(new_program, "seed");
    err = glGetError(); SOFT_ASSERT(!err);

    if (*program && err == 0)
    {
        glDeleteProgram(*program);
    }
    *program = new_program;
}

void build_graphics_program(GLuint* program, GLuint* locations, char* vertex_file, char* fragment_file)
{
    GLuint err = glGetError(); SOFT_ASSERT(!err);
    GLuint vertex_shader = 0, fragment_shader = 0;
    load_and_compile_shader(&vertex_shader, GL_VERTEX_SHADER, vertex_file);
    err = glGetError(); SOFT_ASSERT(!err);
    load_and_compile_shader(&fragment_shader, GL_FRAGMENT_SHADER, fragment_file);
    err = glGetError(); SOFT_ASSERT(!err);

    GLuint new_program = glCreateProgram();
    err = glGetError(); SOFT_ASSERT(!err);
    link_program(new_program, vertex_shader, 0, 0, 0, fragment_shader, 0);
    err = glGetError(); SOFT_ASSERT(!err);
    if (vertex_shader) glDeleteShader(vertex_shader);
    err = glGetError(); SOFT_ASSERT(!err);
    if (fragment_shader) glDeleteShader(fragment_shader);
    err = glGetError(); SOFT_ASSERT(!err);
    locations[GRAPHICS_IMAGE_LOCATION] = glGetUniformLocation(new_program, "image");
    err = glGetError(); SOFT_ASSERT(!err);

    if (*program && err == 0)
    {
        glDeleteProgram(*program);
    }
    *program = new_program;
}

void build_compute_program(GLuint* program, GLuint* locations, char* file)
{
    GLuint err = glGetError(); SOFT_ASSERT(!err);
    GLuint compute_shader = 0;
    load_and_compile_shader(&compute_shader, GL_COMPUTE_SHADER, file);
    err = glGetError(); SOFT_ASSERT(!err);

    GLuint new_program = glCreateProgram();
    err = glGetError(); SOFT_ASSERT(!err);
    link_program(new_program, 0, 0, 0, 0, 0, compute_shader);
    err = glGetError(); SOFT_ASSERT(!err);
    if (compute_shader) glDeleteShader(compute_shader);
    err = glGetError(); SOFT_ASSERT(!err);
    locations[COMPUTE_SPHERES_COUNT_LOCATION] = glGetUniformLocation(new_program, "spheres_count");
    err = glGetError(); SOFT_ASSERT(!err);
    locations[COMPUTE_TRIANGLES_COUNT_LOCATION] = glGetUniformLocation(new_program, "triangles_count");
    err = glGetError(); SOFT_ASSERT(!err);
    locations[COMPUTE_LENSES_COUNT_LOCATION] = glGetUniformLocation(new_program, "lenses_count");
    err = glGetError(); SOFT_ASSERT(!err);
    locations[COMPUTE_AABBS_COUNT_LOCATION] = glGetUniformLocation(new_program, "aabbs_count");
    err = glGetError(); SOFT_ASSERT(!err);
    locations[COMPUTE_SEED_LOCATION] = glGetUniformLocation(new_program, "seed");
    err = glGetError(); SOFT_ASSERT(!err);
    locations[COMPUTE_ROTATION_LOCATION] = glGetUniformLocation(new_program, "rotation");
    err = glGetError(); SOFT_ASSERT(!err);

    if (*program && err == 0)
    {
        glDeleteProgram(*program);
    }
    *program = new_program;
}

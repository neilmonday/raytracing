#include "texture.h"

GLubyte uTGAcompare[11] = { 0,0,2,0,0,0,0,0,0,0,0 }; //some need the last entry to be 4 others 8

Texture LoadTable()
{
    Texture texture;
    GLuint status = 0;

    status |= LoadTGA(&texture, "../../textures/wood.tga");

    if (status == 0)
    {
        // Typical Texture Generation Using Data From The TGA ( CHANGE )
        glGenTextures(1, &texture.texID);                // Create The Texture ( CHANGE )
        glBindTexture(GL_TEXTURE_2D, texture.texID);

        glTexImage2D(GL_TEXTURE_2D, 0, texture.internalFormat, texture.width, texture.height, 0, texture.format, texture.type, texture.imageData);
        if (texture.imageData)                        // If Texture Image Exists ( CHANGE )
        {
            free(texture.imageData);                    // Free The Texture Image Memory ( CHANGE )
        }

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
    }
    return texture;
}

Texture LoadSkybox()
{
    Texture textures[6];
    GLuint status = 0;
#if 0
    status |= LoadTGA(&textures[0], "../../textures/amethyst/amethystft.tga");
    status |= LoadTGA(&textures[1], "../../textures/amethyst/amethystbk.tga");
    status |= LoadTGA(&textures[2], "../../textures/amethyst/amethystup.tga");
    status |= LoadTGA(&textures[3], "../../textures/amethyst/amethystdn.tga");
    status |= LoadTGA(&textures[4], "../../textures/amethyst/amethystrt.tga");
    status |= LoadTGA(&textures[5], "../../textures/amethyst/amethystlf.tga");
#elif 0
    status |= LoadTGA(&textures[0], "../../textures/winterplanet/winterplanetft.tga");
    status |= LoadTGA(&textures[1], "../../textures/winterplanet/winterplanetbk.tga");
    status |= LoadTGA(&textures[2], "../../textures/winterplanet/winterplanetup.tga");
    status |= LoadTGA(&textures[3], "../../textures/winterplanet/winterplanetdn.tga");
    status |= LoadTGA(&textures[4], "../../textures/winterplanet/winterplanetrt.tga");
    status |= LoadTGA(&textures[5], "../../textures/winterplanet/winterplanetlf.tga");
#else
    status |= LoadTGA(&textures[0], "../../textures/black.tga");
    status |= LoadTGA(&textures[1], "../../textures/black.tga");
    status |= LoadTGA(&textures[2], "../../textures/black.tga");
    status |= LoadTGA(&textures[3], "../../textures/black.tga");
    status |= LoadTGA(&textures[4], "../../textures/black.tga");
    status |= LoadTGA(&textures[5], "../../textures/black.tga");
#endif
    if (status == 0)
    {
        // Typical Texture Generation Using Data From The TGA ( CHANGE )
        glGenTextures(1, &textures[0].texID);                // Create The Texture ( CHANGE )
        glBindTexture(GL_TEXTURE_CUBE_MAP, textures[0].texID);
        int i;
        for ( i = 0; i<6; i++)                        // Loop Through Both Textures
        {
            glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, textures[i].internalFormat, textures[i].width, textures[i].height, 0, textures[i].format, textures[i].type, textures[i].imageData);
            if (textures[i].imageData)                        // If Texture Image Exists ( CHANGE )
            {
                free(textures[i].imageData);                    // Free The Texture Image Memory ( CHANGE )
            }
        }
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
    }
    return textures[0];
}

Texture CreateImage(GLuint width, GLuint height)
{
    Texture texture;
    texture.bpp = 32;
    texture.width = width;
    texture.height = height;
    texture.internalFormat = GL_RGBA32F;
    texture.format = GL_RGBA;
    texture.type = GL_FLOAT;
    glGenTextures(1, &texture.texID);
    glBindTexture(GL_TEXTURE_2D, texture.texID);
    glTexImage2D(GL_TEXTURE_2D, 0, texture.internalFormat, texture.width, texture.height, 0, texture.format, texture.type, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    return texture;
}

void UpdateImage(Texture texture)
{
    glTexImage2D(GL_TEXTURE_3D, 0, texture.internalFormat, texture.width, texture.height, 0, texture.format, texture.type, 0);
}

GLuint LoadTGA(Texture * texture, char * filename)
{
    FILE * fTGA;
    fTGA = fopen(filename, "rb");
    if (fTGA == NULL)
    {
        return -1;
    }
    TGAHeader tgaheader;
    if (fread(&tgaheader, sizeof(TGAHeader), 1, fTGA) == 0)
    {
        if (fTGA != NULL)
        {
            fclose(fTGA);
        }
        return -1;
    }

    // using size of uTGAcompare, because I don't want to actually look at the last element.
    if (memcmp(uTGAcompare, &tgaheader, sizeof(uTGAcompare)) == 0)
    {
        LoadUncompressedTGA(texture, filename, fTGA);
    }
    else
    {
        fclose(fTGA);
        return -1;
    }
    return 0;
}

GLuint LoadUncompressedTGA(Texture * texture, char * filename, FILE * fTGA)    // Load an uncompressed TGA (note, much of this code is based on NeHe's
{
    TGA tga;
    if (fread(tga.header, sizeof(tga.header), 1, fTGA) == 0)
    {
        if (fTGA != NULL)
        {
            fclose(fTGA);
        }
        return -1;
    }
    texture->width = tga.header[1] * 256 + tga.header[0];
    texture->height = tga.header[3] * 256 + tga.header[2];
    texture->bpp = tga.header[4];

    tga.Width = texture->width;
    tga.Height = texture->height;
    tga.Bpp = texture->bpp;
    if ((texture->width <= 0) || (texture->height <= 0) || ((texture->bpp != 24) && (texture->bpp != 32)))
    {
        if (fTGA != NULL)
        {
            fclose(fTGA);
        }
        return -1;
    }

    if (texture->bpp == 24)
    {
        texture->internalFormat = GL_RGB;
        texture->format = GL_RGB;
    }
    else
    {
        texture->internalFormat = GL_RGBA;
        texture->format = GL_RGBA;
    }

    texture->type = GL_UNSIGNED_BYTE;

    tga.bytesPerPixel = (tga.Bpp / 8);
    tga.imageSize = (tga.bytesPerPixel * tga.Width * tga.Height);
    texture->imageData = (GLubyte *)malloc(tga.imageSize);
    if (texture->imageData == NULL)
    {
        fclose(fTGA);
        return -1;
    }
    if (fread(texture->imageData, 1, tga.imageSize, fTGA) != tga.imageSize)
    {
        if (texture->imageData != NULL)
        {
            free(texture->imageData);
        }
        fclose(fTGA);
        return -1;
    }
    // Byte Swapping Optimized By Steve Thomas
    for (GLuint cswap = 0; cswap < (int)tga.imageSize; cswap += tga.bytesPerPixel)
    {
        texture->imageData[cswap] ^= texture->imageData[cswap + 2] ^=
            texture->imageData[cswap] ^= texture->imageData[cswap + 2];
    }
    fclose(fTGA);
    return 0;
}

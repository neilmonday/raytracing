#define PI 3.1415926535897932384626433832795028841971f

#include <stdio.h>
#include <math.h>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "json.h"
#include "output.h"
#include "program.h"
#include "scene.h"
#include "texture.h"
#include "utility.h"

Scene scene;

vec3 main_rotation = { 0.0f, 0.0f, 0.0f };
vec3 lenses_rotation = { 0.0f, 0.0f, 0.0f };

//void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods);
void main_cursor_callback(GLFWwindow* window, double x, double y);
void lenses_cursor_callback(GLFWwindow* window, double x, double y);
//vec2 rotation;

void init(GLFWwindow* window, Camera* camera, GLuint* vao, GLuint unit)
{
    GLenum err;

    glfwMakeContextCurrent(window);
    err = glGetError(); SOFT_ASSERT(!err);

    glCreateVertexArrays(1, vao);
    err = glGetError(); SOFT_ASSERT(!err);
    glActiveTexture(GL_TEXTURE0 + unit);
    err = glGetError(); SOFT_ASSERT(!err);
    Texture image = CreateImage(camera->columns, camera->rows);
    err = glGetError(); SOFT_ASSERT(!err);
    glBindImageTexture(unit, image.texID, 0, GL_FALSE, 0, GL_READ_WRITE, GL_RGBA32F);
    err = glGetError(); SOFT_ASSERT(!err);
    glBindTexture(GL_TEXTURE_2D, image.texID);
    err = glGetError(); SOFT_ASSERT(!err);

    //graphics portion
    GLfloat vertices[] = {
        -1.0f, -1.0f,
         1.0f, -1.0f,
         1.0f,  1.0f,
        -1.0f,  1.0f
    };

    GLuint indices[] = {
        0, 1, 2,
        0, 2, 3
    };
    
    GLuint vertices_count = 4;
    GLuint indices_count = 6;

    //creating
    GLuint vbo;
    GLuint ibo;
    glCreateBuffers(1, &vbo);
    err = glGetError(); SOFT_ASSERT(!err);
    glNamedBufferStorage(vbo, vertices_count * sizeof(GLfloat) * 2, vertices, 0);
    err = glGetError(); SOFT_ASSERT(!err);
    glCreateBuffers(1, &ibo);
    err = glGetError(); SOFT_ASSERT(!err);
    glNamedBufferStorage(ibo, indices_count * sizeof(GLuint), indices, 0);
    err = glGetError(); SOFT_ASSERT(!err);

    //connecting
    glVertexArrayVertexBuffer(*vao, 0, vbo, 0, 2 * sizeof(GLfloat));
    err = glGetError(); SOFT_ASSERT(!err);
    glVertexArrayElementBuffer(*vao, ibo);
    err = glGetError(); SOFT_ASSERT(!err);
    glEnableVertexArrayAttrib(*vao, 0);      //0 corresponds to the shader's layout(location=0)
    err = glGetError(); SOFT_ASSERT(!err);
    glVertexArrayAttribFormat(*vao, 0, 2, GL_FLOAT, GL_FALSE, 0);
    err = glGetError(); SOFT_ASSERT(!err);
    glVertexArrayAttribBinding(*vao, 0, 0);
    err = glGetError(); SOFT_ASSERT(!err);
}

void main(void)
{
    GLenum err;
    init_scene(&scene);

    //char* input = "../../input/testSphereScene.json";
    //char* input = "../../input/teapot.json";
    //char* input = "../../input/csg.json";
    //char* input = "../../input/cornell.json";
    char* input = "../../input/lens.json";
    parse_scene(input, &scene);

    const unsigned int rays_per_pixel_x = 8;
    const unsigned int rays_per_pixel_y = 8;
    const unsigned int iterations_per_pixel = 1; // make this 3 for debug

    const unsigned int local_size_x = 8;
    const unsigned int local_size_y = 8;
    const unsigned int local_size_z = 1;

    unsigned int num_groups_x = scene.main_camera.columns / local_size_x;
    unsigned int num_groups_y = scene.main_camera.rows / local_size_y;
    unsigned int num_groups_z = 1 / local_size_z;

    FILE* output = fopen("../../output/rayBundle.json", "w");
    begin_output(output);

    GLFWwindow* main_window;
    GLFWwindow* lenses_window;
    // Initialise GLFW
    if (!glfwInit())
        return;

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    main_window = glfwCreateWindow(scene.main_camera.columns, scene.main_camera.rows, "Raytracing", NULL, NULL);
    lenses_window = glfwCreateWindow(scene.lenses_camera.columns, scene.lenses_camera.rows, "Raytracing Lenses", NULL, main_window);

    glfwSetInputMode(lenses_window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    glfwSetCursorPosCallback(lenses_window, &lenses_cursor_callback);
    glfwSetInputMode(main_window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    glfwSetCursorPosCallback(main_window, &main_cursor_callback);

    //window must be current before initializing glew.
    glfwMakeContextCurrent(main_window);

    // Initialize GLEW
    glewExperimental = GL_TRUE;
    err = glewInit();

    if (GLEW_OK != err)
        return;

    GLuint vao[2];
    err = glGetError(); SOFT_ASSERT(!err);
    init(main_window, &scene.main_camera, &vao[0], 0);
    err = glGetError(); SOFT_ASSERT(!err);
    init(lenses_window, &scene.lenses_camera, &vao[1], 1);

    //generate primary rays
    GLuint primary_ray_compute_program = 0;
    GLuint primary_ray_compute_uniform_locations[RAY_GENERATION_UNIFORM_LOCATIONS_COUNT];
    build_primary_ray_program(&primary_ray_compute_program, primary_ray_compute_uniform_locations, "../../shaders/primary_ray.glsl"); err = glGetError(); SOFT_ASSERT(!err);
    glUseProgram(primary_ray_compute_program); err = glGetError(); SOFT_ASSERT(!err);
    glUniform1ui(primary_ray_compute_uniform_locations[COMPUTE_LENSES_COUNT_LOCATION], scene.lenses_count); err = glGetError(); SOFT_ASSERT(!err);

    //main graphics program
    glfwMakeContextCurrent(main_window);
    GLuint main_graphics_uniform_locations[GRAPHICS_UNIFORM_LOCATIONS_COUNT];
    GLuint main_graphics_program = 0;
    build_graphics_program(&main_graphics_program, main_graphics_uniform_locations, "../../shaders/vertex.glsl", "../../shaders/fragment.glsl");

    //main compute program
    GLuint main_compute_program = 0;
    GLuint main_compute_uniform_locations[COMPUTE_UNIFORM_LOCATIONS_COUNT];
    build_compute_program(&main_compute_program, main_compute_uniform_locations, "../../shaders/compute.glsl"); err = glGetError(); SOFT_ASSERT(!err);
    glUseProgram(main_compute_program); err = glGetError(); SOFT_ASSERT(!err);
    glUniform1ui(main_compute_uniform_locations[COMPUTE_AABBS_COUNT_LOCATION], scene.aabbs_count); err = glGetError(); SOFT_ASSERT(!err);
    glUniform1ui(main_compute_uniform_locations[COMPUTE_SPHERES_COUNT_LOCATION], scene.spheres_count);  err = glGetError(); SOFT_ASSERT(!err);
    glUniform1ui(main_compute_uniform_locations[COMPUTE_TRIANGLES_COUNT_LOCATION], scene.triangles_count); err = glGetError(); SOFT_ASSERT(!err);
    glUniform1ui(main_compute_uniform_locations[COMPUTE_LENSES_COUNT_LOCATION], scene.lenses_count); err = glGetError(); SOFT_ASSERT(!err);

    //lenses graphics program;
    glfwMakeContextCurrent(lenses_window);
    GLuint lenses_graphics_uniform_locations[GRAPHICS_UNIFORM_LOCATIONS_COUNT];
    GLuint lenses_graphics_program = 0;
    build_graphics_program(&lenses_graphics_program, lenses_graphics_uniform_locations, "../../shaders/lenses_vertex.glsl", "../../shaders/lenses_fragment.glsl");

    //lenses compute program
    GLuint lenses_compute_program = 0;
    GLuint lenses_compute_uniform_locations[COMPUTE_UNIFORM_LOCATIONS_COUNT];
    build_compute_program(&lenses_compute_program, lenses_compute_uniform_locations, "../../shaders/lenses_compute.glsl"); err = glGetError(); SOFT_ASSERT(!err);
    glUseProgram(lenses_compute_program); err = glGetError(); SOFT_ASSERT(!err);
    glUniform1ui(lenses_compute_uniform_locations[COMPUTE_AABBS_COUNT_LOCATION], scene.aabbs_count); err = glGetError(); SOFT_ASSERT(!err);
    glUniform1ui(lenses_compute_uniform_locations[COMPUTE_SPHERES_COUNT_LOCATION], scene.spheres_count);  err = glGetError(); SOFT_ASSERT(!err);
    glUniform1ui(lenses_compute_uniform_locations[COMPUTE_TRIANGLES_COUNT_LOCATION], scene.triangles_count); err = glGetError(); SOFT_ASSERT(!err);
    glUniform1ui(lenses_compute_uniform_locations[COMPUTE_LENSES_COUNT_LOCATION], scene.lenses_count); err = glGetError(); SOFT_ASSERT(!err);
    err = glGetError(); SOFT_ASSERT(!err);

    glfwMakeContextCurrent(main_window);
    //create buffers
    GLuint buffers[BUFFERS_COUNT];
    glGenBuffers(BUFFERS_COUNT, buffers);
    err = glGetError(); SOFT_ASSERT(!err);
    //UBOs are one of only a few indexable buffer targets.
    glBindBufferBase(GL_UNIFORM_BUFFER, 2, buffers[MAIN_CAMERA_BUFFER]);
    err = glGetError(); SOFT_ASSERT(!err);
    glBufferData(GL_UNIFORM_BUFFER, sizeof(Camera), &(scene.main_camera), GL_DYNAMIC_DRAW);
    err = glGetError(); SOFT_ASSERT(!err);

    //SSBOs are one of only a few indexable buffer targets.
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 4, buffers[AABBS_BUFFER]);
    err = glGetError(); SOFT_ASSERT(!err);
    glBufferData(GL_SHADER_STORAGE_BUFFER, scene.aabbs_count * sizeof(AABB), scene.aabbs, GL_DYNAMIC_DRAW);
    err = glGetError(); SOFT_ASSERT(!err);
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 5, buffers[SPHERES_BUFFER]);
    err = glGetError(); SOFT_ASSERT(!err);
    glBufferData(GL_SHADER_STORAGE_BUFFER, scene.spheres_count * sizeof(Sphere), scene.spheres, GL_DYNAMIC_DRAW);
    err = glGetError(); SOFT_ASSERT(!err);
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 6, buffers[TRIANGLES_BUFFER]);
    err = glGetError(); SOFT_ASSERT(!err);
    glBufferData(GL_SHADER_STORAGE_BUFFER, scene.triangles_count * sizeof(Triangle), scene.triangles, GL_DYNAMIC_DRAW);
    err = glGetError(); SOFT_ASSERT(!err);

    //generate primary ray
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 8, buffers[OUTPUT_BUFFER]);
    err = glGetError(); SOFT_ASSERT(!err);
    glBufferData(GL_SHADER_STORAGE_BUFFER, iterations_per_pixel * rays_per_pixel_x * rays_per_pixel_y * scene.main_camera.columns * scene.main_camera.rows * sizeof(GLfloat) * 4 * 2, NULL, GL_DYNAMIC_DRAW);

    //lenses
    glfwMakeContextCurrent(lenses_window);
    glBindBufferBase(GL_UNIFORM_BUFFER, 3, buffers[LENSES_CAMERA_BUFFER]);
    err = glGetError(); SOFT_ASSERT(!err);
    glBufferData(GL_UNIFORM_BUFFER, sizeof(Camera), &(scene.lenses_camera), GL_DYNAMIC_DRAW);
    err = glGetError(); SOFT_ASSERT(!err);
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 7, buffers[LENSES_BUFFER]);
    err = glGetError(); SOFT_ASSERT(!err);
    glBufferData(GL_SHADER_STORAGE_BUFFER, scene.lenses_count * sizeof(Lens), scene.lenses, GL_DYNAMIC_DRAW);
    err = glGetError(); SOFT_ASSERT(!err);

    vec4 initial_at = scene.main_camera.at;
    vec4 initial_eye = scene.main_camera.eye;

    HANDLE dwChangeHandle = init_watch_folder("../../shaders/");
    float seed = 0.0f;

    glClearColor(0.3f, 0.5f, 0.8f, 1.0f);

    output_axes_raybundle(output);
    fprintf(output, ",\n");

    glUseProgram(primary_ray_compute_program);
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 8, buffers[OUTPUT_BUFFER]);
    glDispatchCompute(num_groups_x, num_groups_y, num_groups_z);
    //GLfloat* data = glMapBuffer(GL_SHADER_STORAGE_BUFFER, GL_READ_ONLY);
    err = glGetError(); SOFT_ASSERT(!err);
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
    err = glGetError(); SOFT_ASSERT(!err);

    //output_raybundle(output, scene.main_camera.columns * scene.main_camera.rows * rays_per_pixel_x * rays_per_pixel_y * iterations_per_pixel, data);
    end_output(output);
    fclose(output);

    // Loop until the user closes the window 
    while (!glfwWindowShouldClose(main_window) && !glfwWindowShouldClose(lenses_window))
    {
        Sleep(1);
        seed += (float)rand() / (float)RAND_MAX;

        glUseProgram(primary_ray_compute_program);
        glUniform1f(primary_ray_compute_uniform_locations[RAY_GENERATION_SEED_LOCATION], seed);
        glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 8, buffers[OUTPUT_BUFFER]);
        glDispatchCompute(num_groups_x, num_groups_y, num_groups_z);

        glfwMakeContextCurrent(main_window);
        mat4 main_rotation_matrix = rotate(main_rotation);

        //main_forward = multiply_mat4_vec4(&main_rotation_matrix, &main_forward);
        //scene.main_camera.at = subtract_vec4(scene.main_camera.eye, main_forward);
        calculate_translation(main_window, main_rotation, &scene.main_camera.eye);
        calculate_translation(main_window, main_rotation, &scene.main_camera.at);
        err = glGetError(); SOFT_ASSERT(!err);
        glClear(GL_COLOR_BUFFER_BIT);
        err = glGetError(); SOFT_ASSERT(!err);
        glUseProgram(main_compute_program);
        err = glGetError(); SOFT_ASSERT(!err);
        glUniformMatrix4fv(main_compute_uniform_locations[COMPUTE_ROTATION_LOCATION], 1, 0, &main_rotation_matrix);
        err = glGetError(); SOFT_ASSERT(!err);
        glActiveTexture(GL_TEXTURE0 + 0);
        err = glGetError(); SOFT_ASSERT(!err);
        glBindBufferBase(GL_UNIFORM_BUFFER, 2, buffers[MAIN_CAMERA_BUFFER]);
        err = glGetError(); SOFT_ASSERT(!err);
        glBufferData(GL_UNIFORM_BUFFER, sizeof(Camera), &(scene.main_camera), GL_DYNAMIC_DRAW);
        err = glGetError(); SOFT_ASSERT(!err);
        glUniform1f(main_compute_uniform_locations[COMPUTE_SEED_LOCATION], seed);
        err = glGetError(); SOFT_ASSERT(!err);
        glDispatchCompute(num_groups_x, num_groups_y, num_groups_z);
        err = glGetError(); SOFT_ASSERT(!err);
        glUseProgram(main_graphics_program);
        err = glGetError(); SOFT_ASSERT(!err);
        glBindVertexArray(vao[0]);
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, NULL);
        err = glGetError(); SOFT_ASSERT(!err);
        glfwSwapBuffers(main_window);
        err = glGetError(); SOFT_ASSERT(!err);

        glfwMakeContextCurrent(lenses_window);
        vec4 lenses_forward = { 0.0f, 0.0f, 1.0f, 1.0f };
        mat4 lenses_rotation_matrix = rotate(lenses_rotation);

        lenses_forward = multiply_mat4_vec4(&lenses_rotation_matrix, &lenses_forward);
        scene.lenses_camera.at = subtract_vec4(scene.lenses_camera.eye, lenses_forward);
        calculate_translation(lenses_window, lenses_rotation, &scene.lenses_camera.eye);
        calculate_translation(lenses_window, lenses_rotation, &scene.lenses_camera.at);

        err = glGetError(); SOFT_ASSERT(!err);
        glClear(GL_COLOR_BUFFER_BIT);
        err = glGetError(); SOFT_ASSERT(!err);
        glUseProgram(lenses_compute_program);
        glActiveTexture(GL_TEXTURE0 + 1);
        err = glGetError(); SOFT_ASSERT(!err);
        glBindBufferBase(GL_UNIFORM_BUFFER, 3, buffers[LENSES_CAMERA_BUFFER]);
        err = glGetError(); SOFT_ASSERT(!err);
        glBufferData(GL_UNIFORM_BUFFER, sizeof(Camera), &(scene.lenses_camera), GL_DYNAMIC_DRAW);
        err = glGetError(); SOFT_ASSERT(!err);
        glUniform1f(lenses_compute_uniform_locations[COMPUTE_SEED_LOCATION], seed);
        err = glGetError(); SOFT_ASSERT(!err);
        glDispatchCompute(num_groups_x, num_groups_y, num_groups_z);
        err = glGetError(); SOFT_ASSERT(!err);
        glUseProgram(lenses_graphics_program);
        err = glGetError(); SOFT_ASSERT(!err);
        glBindVertexArray(vao[1]);
        err = glGetError(); SOFT_ASSERT(!err);
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, NULL);
        err = glGetError(); SOFT_ASSERT(!err);
        glfwSwapBuffers(lenses_window);
        err = glGetError(); SOFT_ASSERT(!err);
        glfwPollEvents();

        //watch_folder(dwChangeHandle, &build_compute_program, &compute_program, compute_uniform_locations);
    }
    return;
}

void main_cursor_callback(GLFWwindow* window, double x, double y)
{
    static unsigned int first_time = 1;
    static double initial_x = -6.28f;
    static double initial_y = 0.0f;

    if (first_time)
    {
        first_time = 0;
        initial_x = initial_x - (x / 300.0);
        initial_y = initial_y - (y / 300.0);
    }

    main_rotation.y = initial_x + (x / 300.0);  //hack. this needs to be negative for some reason.
    main_rotation.x = initial_y + (y / 300.0);

    if (main_rotation.x > 1.570)
        main_rotation.x = 1.570;
    if (main_rotation.x < -1.570)
        main_rotation.x = -1.570;
    /*printf("*****************\n");
    printf("Mouse Up:\n");
    printf("    Cursor : %f, %f\n", cursor.x, cursor.y);
    printf("  Rotation : %f, %f\n", rotation.x, rotation.y);
    printf("*****************\n");*/
}

void lenses_cursor_callback(GLFWwindow* window, double x, double y)
{
    static unsigned int first_time = 1;
    static double initial_x = -4.84f;
    static double initial_y = 0.0f;

    if (first_time)
    {
        first_time = 0;
        initial_x = initial_x - (x / 300.0);
        initial_y = initial_y - (y / 300.0);
    }

    lenses_rotation.y = initial_x + (x / 300.0);
    lenses_rotation.x = initial_y + (y / 300.0);

    if (lenses_rotation.x > 1.570)
        lenses_rotation.x = 1.570;
    if (lenses_rotation.x < -1.570)
        lenses_rotation.x = -1.570;
    /*printf("*****************\n");
    printf("Mouse Up:\n");
    printf("    Cursor : %f, %f\n", cursor.x, cursor.y);
    printf("  Rotation : %f, %f\n", rotation.x, rotation.y);
    printf("*****************\n");*/
}

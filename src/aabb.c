#include "aabb.h"

void init_aabb(AABB *aabb) {
    aabb->min.x = 0.0f;
    aabb->min.y = 0.0f;
    aabb->min.z = 0.0f;
    aabb->min.w = 1.0f;
    aabb->max.x = 0.0f;
    aabb->max.y = 0.0f;
    aabb->max.z = 0.0f;
    aabb->max.w = 1.0f;
}

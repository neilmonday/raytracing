#include "lens.h"

void init_lens(Lens *lens){
    lens->cylinder_radius = 0.0f;
    lens->cylinder_z1 = 0.0f;
    lens->cylinder_z2 = 0.0f;
    lens->radius1 = 0.0f;
    lens->radius2 = 0.0f;
    lens->center_z1 = 0.0f;
    lens->center_z2 = 0.0f;
    lens->refractive_index = 0.0f;
}

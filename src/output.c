#include "output.h"

void begin_output(FILE* file)
{
    fprintf(file, "{\n");
    fprintf(file, "\"rayBundles\" :[\n"); //rayBundles
}

void end_output(FILE* file)
{
    fprintf(file, "]\n"); //end rayBundles
    fprintf(file, "}\n");
    fclose(file);
}

void output_axes_raybundle(FILE* file) 
{
    fprintf(file, "[\n"); //rayBundle

    fprintf(file, "%f,%f,%f,%f,%f,%f", 0.0, 0.0, 0.0, 1.0, 0.0, 0.0);
    fprintf(file, ",\n");
    fprintf(file, "%f,%f,%f,%f,%f,%f", 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);
    fprintf(file, ",\n");
    fprintf(file, "%f,%f,%f,%f,%f,%f", 0.0, 0.0, 0.0, 0.0, 0.0, 1.0);
    fprintf(file, "\n");

    fprintf(file, "]\n"); //end rayBundle
}

void output_camera_raybundle(FILE* file, Camera camera, vec3 u, vec3 v, vec2 view_plane_increment)
{
    vec2 extents[4];
    extents[0].x = (camera.columns - 1) - (camera.columns >> 1) + 0.5;
    extents[0].y = (camera.rows - 1) - (camera.rows >> 1) + 0.5;
    extents[1].x = 0 - (camera.columns >> 1) + 0.5;
    extents[1].y = (camera.rows - 1) - (camera.rows >> 1) + 0.5;
    extents[2].x = 0 - (camera.columns >> 1) + 0.5;
    extents[2].y = 0 - (camera.rows >> 1) + 0.5;
    extents[3].x = (camera.columns - 1) - (camera.columns >> 1) + 0.5;
    extents[3].y = 0 - (camera.rows >> 1) + 0.5;
    int i = 0;

    GLfloat points[32];
    for (i = 0; i < 4; i++)
    {
        vec3 u_prime = multiply(u, extents[i].x * view_plane_increment.x);
        vec3 v_prime = multiply(v, extents[i].y * view_plane_increment.y);
        vec3 camera_at, camera_eye;
        memcpy(&camera_at, &(camera.at), sizeof(vec3));
        memcpy(&camera_eye, &(camera.eye), sizeof(vec3));

        vec3 result = add(add(u_prime, v_prime), subtract(camera_at, camera_eye));
        points[(i * 8) + 0] = camera.eye.x;
        points[(i * 8) + 1] = camera.eye.y;
        points[(i * 8) + 2] = camera.eye.z;
        points[(i * 8) + 3] = 1.0f;
        points[(i * 8) + 4] = camera.eye.x + result.x;
        points[(i * 8) + 5] = camera.eye.y + result.y;
        points[(i * 8) + 6] = camera.eye.z + result.z;
        points[(i * 8) + 7] = 1.0f;
    }

    fprintf(file, "[\n"); //rayBundle

    fprintf(file, "%f,%f,%f,%f,%f,%f",
        points[(0 * 8) + 0],
        points[(0 * 8) + 1],
        points[(0 * 8) + 2],
        points[(0 * 8) + 4],
        points[(0 * 8) + 5],
        points[(0 * 8) + 6]);
    fprintf(file, ",\n");
    fprintf(file, "%f,%f,%f,%f,%f,%f",
        points[(1 * 8) + 0],
        points[(1 * 8) + 1],
        points[(1 * 8) + 2],
        points[(1 * 8) + 4],
        points[(1 * 8) + 5],
        points[(1 * 8) + 6]);
    fprintf(file, ",\n");
    fprintf(file, "%f,%f,%f,%f,%f,%f",
        points[(2 * 8) + 0],
        points[(2 * 8) + 1],
        points[(2 * 8) + 2],
        points[(2 * 8) + 4],
        points[(2 * 8) + 5],
        points[(2 * 8) + 6]);
    fprintf(file, ",\n");
    fprintf(file, "%f,%f,%f,%f,%f,%f",
        points[(3 * 8) + 0],
        points[(3 * 8) + 1],
        points[(3 * 8) + 2],
        points[(3 * 8) + 4],
        points[(3 * 8) + 5],
        points[(3 * 8) + 6]);
    fprintf(file, ",\n");

    fprintf(file, "%f,%f,%f,%f,%f,%f",
        points[(1 * 8) + 4],
        points[(1 * 8) + 5],
        points[(1 * 8) + 6],
        points[(0 * 8) + 4],
        points[(0 * 8) + 5],
        points[(0 * 8) + 6]);
    fprintf(file, ",\n");
    fprintf(file, "%f,%f,%f,%f,%f,%f",
        points[(2 * 8) + 4],
        points[(2 * 8) + 5],
        points[(2 * 8) + 6],
        points[(1 * 8) + 4],
        points[(1 * 8) + 5],
        points[(1 * 8) + 6]);
    fprintf(file, ",\n");
    fprintf(file, "%f,%f,%f,%f,%f,%f",
        points[(3 * 8) + 4],
        points[(3 * 8) + 5],
        points[(3 * 8) + 6],
        points[(2 * 8) + 4],
        points[(2 * 8) + 5],
        points[(2 * 8) + 6]);
    fprintf(file, ",\n");
    fprintf(file, "%f,%f,%f,%f,%f,%f",
        points[(0 * 8) + 4],
        points[(0 * 8) + 5],
        points[(0 * 8) + 6],
        points[(3 * 8) + 4],
        points[(3 * 8) + 5],
        points[(3 * 8) + 6]);

    fprintf(file, "]\n"); //end rayBundle
}

void output_raybundle(FILE* file, GLuint count, GLfloat* data) 
{
    fprintf(file, "[\n"); //rayBundle
    int first = 1;
    int i = 0;
    for (i = 0; i < count; i++)
    {
        //skip zero length segments.
        if (data[(i * 8) + 0] != data[(i * 8) + 4] || 
            data[(i * 8) + 1] != data[(i * 8) + 5] ||
            data[(i * 8) + 2] != data[(i * 8) + 6])
        {
            if ((data[(i * 8) + 0] != data[(i * 8) + 0]) ||
                (data[(i * 8) + 4] != data[(i * 8) + 4]) ||
                (data[(i * 8) + 1] != data[(i * 8) + 1]) ||
                (data[(i * 8) + 5] != data[(i * 8) + 5]) ||
                (data[(i * 8) + 2] != data[(i * 8) + 2]) ||
                (data[(i * 8) + 6] != data[(i * 8) + 6]))
            {
                continue;
            }

            if (!first)
            {
                fprintf(file, ",\n");
            }

            first = 0;
            fprintf(file, "%f,%f,%f,%f,%f,%f",
                data[(i * 8) + 0],
                data[(i * 8) + 1],
                data[(i * 8) + 2],
                data[(i * 8) + 4],
                data[(i * 8) + 5],
                data[(i * 8) + 6]);
        }
    }
    fprintf(file, "]\n"); //end rayBundle
}

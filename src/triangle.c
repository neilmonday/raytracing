#include "triangle.h"

void init_triangle(Triangle *triangle){
    triangle->v0.x = 0.0f;
    triangle->v0.y = 0.0f;
    triangle->v0.z = 0.0f;
    triangle->v0.w = 1.0f;
    triangle->v1.x = 0.0f;
    triangle->v1.y = 0.0f;
    triangle->v1.z = 0.0f;
    triangle->v1.w = 1.0f;
    triangle->v2.x = 0.0f;
    triangle->v2.y = 0.0f;
    triangle->v2.z = 0.0f;
    triangle->v2.w = 1.0f;

    triangle->n0.x = 0.0f;
    triangle->n0.y = 0.0f;
    triangle->n0.z = 0.0f;
    triangle->n0.w = 1.0f;
    triangle->n1.x = 0.0f;
    triangle->n1.y = 0.0f;
    triangle->n1.z = 0.0f;
    triangle->n1.w = 1.0f;
    triangle->n2.x = 0.0f;
    triangle->n2.y = 0.0f;
    triangle->n2.z = 0.0f;
    triangle->n2.w = 1.0f;

    triangle->padding1.x = 0.0f;
    triangle->padding1.y = 0.0f;
    triangle->padding1.z = 0.0f;
    triangle->padding1.w = 0.0f;
    triangle->padding2.x = 0.0f;
    triangle->padding2.y = 0.0f;
    triangle->padding2.z = 0.0f;
    triangle->padding2.w = 0.0f;
}

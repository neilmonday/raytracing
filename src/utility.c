#include "utility.h"

const double PI = 3.1415926535897932384626433832795;
const double E = 2.7182818284590452353602874713526;

time_t last_time;

mat4 multiply_mat4(mat4*const A, mat4*const B)
{
    mat4 result;

    result.x.x = A->x.x * B->x.x + A->x.y * B->y.x + A->x.z * B->z.x + A->x.w * B->w.x;
    result.x.y = A->x.x * B->x.y + A->x.y * B->y.y + A->x.z * B->z.y + A->x.w * B->w.y;
    result.x.z = A->x.x * B->x.z + A->x.y * B->y.z + A->x.z * B->z.z + A->x.w * B->w.z;
    result.x.w = A->x.x * B->x.w + A->x.y * B->y.w + A->x.z * B->z.w + A->x.w * B->w.w;

    result.y.x = A->y.x * B->x.x + A->y.y * B->y.x + A->y.z * B->z.x + A->y.w * B->w.x;
    result.y.y = A->y.x * B->x.y + A->y.y * B->y.y + A->y.z * B->z.y + A->y.w * B->w.y;
    result.y.z = A->y.x * B->x.z + A->y.y * B->y.z + A->y.z * B->z.z + A->y.w * B->w.z;
    result.y.w = A->y.x * B->x.w + A->y.y * B->y.w + A->y.z * B->z.w + A->y.w * B->w.w;

    result.z.x = A->z.x * B->x.x + A->z.y * B->y.x + A->z.z * B->z.x + A->z.w * B->w.x;
    result.z.y = A->z.x * B->x.y + A->z.y * B->y.y + A->z.z * B->z.y + A->z.w * B->w.y;
    result.z.z = A->z.x * B->x.z + A->z.y * B->y.z + A->z.z * B->z.z + A->z.w * B->w.z;
    result.z.w = A->z.x * B->x.w + A->z.y * B->y.w + A->z.z * B->z.w + A->z.w * B->w.w;

    result.w.x = A->w.x * B->x.x + A->w.y * B->y.x + A->w.z * B->z.x + A->w.w * B->w.x;
    result.w.y = A->w.x * B->x.y + A->w.y * B->y.y + A->w.z * B->z.y + A->w.w * B->w.y;
    result.w.z = A->w.x * B->x.z + A->w.y * B->y.z + A->w.z * B->z.z + A->w.w * B->w.z;
    result.w.w = A->w.x * B->x.w + A->w.y * B->y.w + A->w.z * B->z.w + A->w.w * B->w.w;

    return result;
}

mat3 multiply_mat3(mat3*const A, mat3*const B)
{
    mat3 result;

    result.x.x = A->x.x * B->x.x + A->x.y * B->y.x + A->x.z * B->z.x;
    result.x.y = A->x.x * B->x.y + A->x.y * B->y.y + A->x.z * B->z.y;
    result.x.z = A->x.x * B->x.z + A->x.y * B->y.z + A->x.z * B->z.z;

    result.y.x = A->y.x * B->x.x + A->y.y * B->y.x + A->y.z * B->z.x;
    result.y.y = A->y.x * B->x.y + A->y.y * B->y.y + A->y.z * B->z.y;
    result.y.z = A->y.x * B->x.z + A->y.y * B->y.z + A->y.z * B->z.z;

    result.z.x = A->z.x * B->x.x + A->z.y * B->y.x + A->z.z * B->z.x;
    result.z.y = A->z.x * B->x.y + A->z.y * B->y.y + A->z.z * B->z.y;
    result.z.z = A->z.x * B->x.z + A->z.y * B->y.z + A->z.z * B->z.z;

    return result;
}


vec4 multiply_mat4_vec4(mat4*const A, vec4*const B)
{
    vec4 result;

    result.x = A->x.x * B->x + A->x.y * B->y + A->x.z * B->z + A->x.w * B->w;
    result.y = A->y.x * B->x + A->y.y * B->y + A->y.z * B->z + A->y.w * B->w;
    result.z = A->z.x * B->x + A->z.y * B->y + A->z.z * B->z + A->z.w * B->w;
    result.w = A->w.x * B->x + A->w.y * B->y + A->w.z * B->z + A->w.w * B->w;

    return result;
}


vec4 multiply_mat3_vec4(mat3*const A, vec4*const B)
{
    vec4 result;


    result.x = A->x.x * B->x + A->x.y * B->y + A->x.z * B->z;

    result.y = A->y.x * B->x + A->y.y * B->y + A->y.z * B->z;

    result.z = A->z.x * B->x + A->z.y * B->y + A->z.z * B->z;

    result.w = 1.0;

    //printf("\n\nresult: %f, %f, %f, %f\n\n", result.x, result.y, result.z, result.w);

    return result;
}

dmat4 multiply_dmat4(dmat4*const A, dmat4*const B)
{
    dmat4 result;

    result.x.x = A->x.x * B->x.x + A->x.y * B->y.x + A->x.z * B->z.x + A->x.w * B->w.x;
    result.x.y = A->x.x * B->x.y + A->x.y * B->y.y + A->x.z * B->z.y + A->x.w * B->w.y;
    result.x.z = A->x.x * B->x.z + A->x.y * B->y.z + A->x.z * B->z.z + A->x.w * B->w.z;
    result.x.w = A->x.x * B->x.w + A->x.y * B->y.w + A->x.z * B->z.w + A->x.w * B->w.w;

    result.y.x = A->y.x * B->x.x + A->y.y * B->y.x + A->y.z * B->z.x + A->y.w * B->w.x;
    result.y.y = A->y.x * B->x.y + A->y.y * B->y.y + A->y.z * B->z.y + A->y.w * B->w.y;
    result.y.z = A->y.x * B->x.z + A->y.y * B->y.z + A->y.z * B->z.z + A->y.w * B->w.z;
    result.y.w = A->y.x * B->x.w + A->y.y * B->y.w + A->y.z * B->z.w + A->y.w * B->w.w;

    result.z.x = A->z.x * B->x.x + A->z.y * B->y.x + A->z.z * B->z.x + A->z.w * B->w.x;
    result.z.y = A->z.x * B->x.y + A->z.y * B->y.y + A->z.z * B->z.y + A->z.w * B->w.y;
    result.z.z = A->z.x * B->x.z + A->z.y * B->y.z + A->z.z * B->z.z + A->z.w * B->w.z;
    result.z.w = A->z.x * B->x.w + A->z.y * B->y.w + A->z.z * B->z.w + A->z.w * B->w.w;

    result.w.x = A->w.x * B->x.x + A->w.y * B->y.x + A->w.z * B->z.x + A->w.w * B->w.x;
    result.w.y = A->w.x * B->x.y + A->w.y * B->y.y + A->w.z * B->z.y + A->w.w * B->w.y;
    result.w.z = A->w.x * B->x.z + A->w.y * B->y.z + A->w.z * B->z.z + A->w.w * B->w.z;
    result.w.w = A->w.x * B->x.w + A->w.y * B->y.w + A->w.z * B->z.w + A->w.w * B->w.w;

    return result;
}

int identity(mat4* set_identity)
{
    mat4 identity;
    identity.x.x = 1.0f;
    identity.x.y = 0.0f;
    identity.x.z = 0.0f;
    identity.x.w = 0.0f;

    identity.y.x = 0.0f;
    identity.y.y = 1.0f;
    identity.y.z = 0.0f;
    identity.y.w = 0.0f;

    identity.z.x = 0.0f;
    identity.z.y = 0.0f;
    identity.z.z = 1.0f;
    identity.z.w = 0.0f;

    identity.w.x = 0.0f;
    identity.w.y = 0.0f;
    identity.w.z = 0.0f;
    identity.w.w = 1.0f;
    *set_identity = identity;
    return 0;
}

int translate(mat4* translation, float x, float y, float z)
{
    mat4 translate;

    translate.x.x = 1.0f;
    translate.x.y = 0.0f;
    translate.x.z = 0.0f;
    translate.x.w = 0.0f;

    translate.y.x = 0.0f;
    translate.y.y = 1.0f;
    translate.y.z = 0.0f;
    translate.y.w = 0.0f;

    translate.z.x = 0.0f;
    translate.z.y = 0.0f;
    translate.z.z = 1.0f;
    translate.z.w = 0.0f;

    translate.w.x = x;
    translate.w.y = y;
    translate.w.z = z;
    translate.w.w = 1.0f;

    *translation = multiply_mat4(&translate, translation);
    return 0;
}

float length(vec3 A)
{
    float result = sqrt(
        A.x * A.x +
        A.y * A.y +
        A.z * A.z);
    return result;
}

vec3 add(vec3 A, vec3 B)
{
    vec3 result;
    result.x = A.x + B.x;
    result.y = A.y + B.y;
    result.z = A.z + B.z;
    return result;
}

vec3 subtract(vec3 A, vec3 B)
{
    vec3 result;
    result.x = A.x - B.x;
    result.y = A.y - B.y;
    result.z = A.z - B.z;
    return result;
}

vec4 add_vec4(vec4 A, vec4 B)
{
    vec4 result;
    result.x = A.x + B.x;
    result.y = A.y + B.y;
    result.z = A.z + B.z;
    result.w = A.w + B.w;
    return result;
}

vec4 subtract_vec4(vec4 A, vec4 B)
{
    vec4 result;
    result.x = A.x - B.x;
    result.y = A.y - B.y;
    result.z = A.z - B.z;
    result.w = A.w - B.w;
    return result;
}

vec3 multiply(vec3 A, float B)
{
    vec3 result;
    result.x = A.x * B;
    result.y = A.y * B;
    result.z = A.z * B;
    return result;
}

vec3 divide(vec3 A, float B)
{
    vec3 result;
    result.x = A.x / B;
    result.y = A.y / B;
    result.z = A.z / B;
    return result;
}

vec3 normalize(vec3 A)
{
    float l = length(A);
    vec3 result = divide(A, l);
    return result;
}

vec3 cross(vec3 A, vec3 B)
{
    vec3 result;
    result.x = A.y*B.z - A.z*B.y;
    result.y = A.z*B.x - A.x*B.z;
    result.z = A.x*B.y - A.y*B.x;
    return result;
}

mat4 rotate(vec3 rotation)
{
    //i should be able to combine these.
    mat4 rotation_phi;
    identity(&rotation_phi);

    rotation_phi.y.y = (GLfloat)cos(rotation.x);
    rotation_phi.y.z = (GLfloat)sin(rotation.x);

    rotation_phi.z.y = (GLfloat)-sin(rotation.x);
    rotation_phi.z.z = (GLfloat)cos(rotation.x);

    mat4 rotation_theta;
    identity(&rotation_theta);

    rotation_theta.x.x = (GLfloat)cos(rotation.y);
    rotation_theta.x.z = (GLfloat)-sin(rotation.y);

    rotation_theta.z.x = (GLfloat)sin(rotation.y);
    rotation_theta.z.z = (GLfloat)cos(rotation.y);

    mat4 result;
    result = multiply_mat4(&rotation_theta, &rotation_phi);
    return result;
}

void calculate_translation(GLFWwindow* window, const vec3 rotation, vec3* position)
{
    GLdouble sensitivity = 2.5;
    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
    {
        position->x += sensitivity * sin(rotation.y)  *  cos(rotation.x);
        position->y += sensitivity * 1                * -sin(rotation.x);
        position->z += sensitivity * -cos(rotation.y) *  cos(rotation.x);
    }
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
    {
        position->x -= sensitivity * cos(rotation.y);
        //*y -=;
        position->z -= sensitivity * sin(rotation.y);
    }
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
    {
        position->x -= sensitivity * sin(rotation.y)  *  cos(rotation.x);
        position->y -= sensitivity * 1                * -sin(rotation.x);
        position->z -= sensitivity * -cos(rotation.y) *  cos(rotation.x);
    }
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
    {
        position->x += sensitivity * cos(rotation.y);
        //*y += ;
        position->z += sensitivity * sin(rotation.y);
    }
}

HANDLE init_watch_folder( LPTSTR lpDir)
{
    last_time = time(NULL);

    // Wait for notification.
    HANDLE dwChangeHandle;
    TCHAR lpDrive[4];
    TCHAR lpFile[_MAX_FNAME];
    TCHAR lpExt[_MAX_EXT];

    _tsplitpath_s(lpDir, lpDrive, 4, NULL, 0, lpFile, _MAX_FNAME, lpExt, _MAX_EXT);

    lpDrive[2] = (TCHAR)'\\';
    lpDrive[3] = (TCHAR)'\0';

    // Watch the directory for file creation and deletion. 

    dwChangeHandle = FindFirstChangeNotification(
        lpDir,                         // directory to watch 
        TRUE,                         // do not watch subtree 
        FILE_NOTIFY_CHANGE_LAST_WRITE); // watch file name changes 

    if (dwChangeHandle == INVALID_HANDLE_VALUE)
    {
        printf("\n ERROR: FindFirstChangeNotification function failed.\n");
        ExitProcess(GetLastError());
    }

    // Make a final validation check on our handles.

    if (dwChangeHandle == NULL)
    {
        printf("\n ERROR: Unexpected NULL from FindFirstChangeNotification.\n");
        ExitProcess(GetLastError());
    }

    return dwChangeHandle;
}

void watch_folder(HANDLE dwChangeHandle, void(*rebuild)(GLuint*, GLuint*), GLuint* compute_program, GLuint* locations)
{
    DWORD dwWaitStatus = WaitForMultipleObjects(1, &dwChangeHandle, FALSE, 1);
    switch (dwWaitStatus)
    {
    case WAIT_OBJECT_0:
    {
        time_t current_time;
        current_time = time(NULL);
        double diff_time = difftime(current_time, last_time);
        if (diff_time > 1.0)
        {
            printf("file has changed. difftime: %f\n", diff_time);
            Sleep(1000);
            glFinish();
            rebuild(compute_program, locations);

        }
        last_time = current_time;
        if (FindNextChangeNotification(dwChangeHandle) == FALSE)
        {
            printf("\n ERROR: FindNextChangeNotification function failed.\n");
            ExitProcess(GetLastError());
        }
        break;
    }
    case WAIT_TIMEOUT:
        //printf("No changes in the timeout period.\n");
        break;

    default:
        printf("ERROR: Unhandled dwWaitStatus.\n");
        ExitProcess(GetLastError());
        break;
    }
}



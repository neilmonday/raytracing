#include "json.h"

static int jsoneq(const char *json, jsmntok_t *tok, const char *s) {
    if (tok->type == JSMN_STRING && (int)strlen(s) == tok->end - tok->start &&
        strncmp(json + tok->start, s, tok->end - tok->start) == 0) {
        return 0;
    }
    return -1;
}

float parse_float(int* i, jsmntok_t** const t, char* const buffer)
{
    size_t size = (*t)[*i].end - (*t)[*i].start;
    //malloc and free so frequently is dumb.
    char* float_string = (char*)malloc(size + 1);
    memcpy(float_string, buffer + (*t)[*i].start, size);
    float_string[size] = '\0';
    float new_float = (float)atof(float_string);
    free(float_string);
    return new_float;
}

int parse_int(int* i, jsmntok_t** const t, char* const buffer)
{
    size_t size = (*t)[*i].end - (*t)[*i].start;
    //malloc and free so frequently is dumb.
    char* int_string = (char*)malloc(size + 1);
    memcpy(int_string, buffer + (*t)[*i].start, size);
    int_string[size] = '\0';
    float new_int = (float)atoi(int_string);
    free(int_string);
    return (int)new_int;
}

void parse_camera(int* i, jsmntok_t** const t, char* const buffer, Camera* camera)
{
    int object_size = (*t)[*i].size;
    (*i)++;//consume the object {"wavelength":400.0, "power":0.0}, now that we have its size
    int l;
    for (l = 0; l < object_size; l++)
    {
        if (jsoneq(buffer, &((*t)[*i]), "Eye") == 0)
        {
            (*i)++; // consume the string "Eye"
            if ((*t)[*i].type == JSMN_ARRAY)
            {
                (*i)++;
                camera->eye.x = parse_float(i, t, buffer);
                (*i)++;
                camera->eye.y = parse_float(i, t, buffer);
                (*i)++;
                camera->eye.z = parse_float(i, t, buffer);
            }
            (*i)++; // consume the name
        }
        else if (jsoneq(buffer, &((*t)[*i]), "At") == 0)
        {
            (*i)++; // consume the string "Eye"
            if ((*t)[*i].type == JSMN_ARRAY)
            {
                (*i)++;
                camera->at.x = parse_float(i, t, buffer);
                (*i)++;
                camera->at.y = parse_float(i, t, buffer);
                (*i)++;
                camera->at.z = parse_float(i, t, buffer);
            }
            (*i)++; // consume the name
        }
        else if (jsoneq(buffer, &((*t)[*i]), "Up") == 0)
        {
            (*i)++; // consume the string "Eye"
            if ((*t)[*i].type == JSMN_ARRAY)
            {
                (*i)++;
                camera->up.x = parse_float(i, t, buffer);
                (*i)++;
                camera->up.y = parse_float(i, t, buffer);
                (*i)++;
                camera->up.z = parse_float(i, t, buffer);
            }
            (*i)++; // consume the name
        }
        else if (jsoneq(buffer, &((*t)[*i]), "FOV") == 0)
        {
            (*i)++; // consume the string "FOV"
            camera->fov = parse_float(i, t, buffer);
            (*i)++; // consume the FOV
        }
        else if (jsoneq(buffer, &((*t)[*i]), "aspect") == 0)
        {
            (*i)++; // consume the string "aspect"
            camera->aspect = parse_float(i, t, buffer);
            (*i)++; // consume the aspect
        }
        else if (jsoneq(buffer, &((*t)[*i]), "rows") == 0)
        {
            (*i)++; // consume the string "rows"
            camera->columns = parse_int(i, t, buffer);
            (*i)++; // consume the rows
        }
        else if (jsoneq(buffer, &((*t)[*i]), "columns") == 0)
        {
            (*i)++; // consume the string "columns"
            camera->rows = parse_int(i, t, buffer);
            (*i)++; // consume the columns
        }
    }
}

void parse_sphere(int* i, jsmntok_t** const t, char* const buffer, Scene* scene)
{
    if ((((*t)[*i].type == JSMN_STRING) && (jsoneq(buffer, &(*t)[*i], "quadruples") == 0)) || 
        (((*t)[*i].type == JSMN_STRING) && (jsoneq(buffer, &(*t)[*i], "quadruple")  == 0)))
    {
        (*i)++; // consume the string "quadruples"
        if ((*t)[*i].type == JSMN_ARRAY)
        {
            int additional_spheres_count = (*t)[*i].size / 4;
            scene->spheres_count += additional_spheres_count;
            scene->spheres = (Sphere*)realloc(scene->spheres, sizeof(Sphere) * scene->spheres_count);
            (*i)++;
            unsigned int sphere = 0;
            for (sphere = scene->spheres_count - additional_spheres_count; sphere < scene->spheres_count; sphere++)
            {
                scene->spheres[sphere].position.x = parse_float(i, t, buffer);
                (*i)++;
                scene->spheres[sphere].position.y = parse_float(i, t, buffer);
                (*i)++;
                scene->spheres[sphere].position.z = parse_float(i, t, buffer);
                (*i)++;
                scene->spheres[sphere].radius = parse_float(i, t, buffer);
                (*i)++;
            }
        }
    }
}

void parse_aabb(int* i, jsmntok_t** const t, char* const buffer, Scene* scene)
{
    int additional_aabbs_count = 0;
    if ((((*t)[*i].type == JSMN_STRING) && (jsoneq(buffer, &(*t)[*i], "MIN") == 0)) ||
        (((*t)[*i].type == JSMN_STRING) && (jsoneq(buffer, &(*t)[*i], "min") == 0)))
    {
        (*i)++; // consume the string "min"
        if ((*t)[*i].type == JSMN_ARRAY)
        {
            additional_aabbs_count = (*t)[*i].size / 3;
            scene->aabbs_count += additional_aabbs_count;
            scene->aabbs = (AABB*)realloc(scene->aabbs, sizeof(AABB) * scene->aabbs_count);
            (*i)++;//consume the array [
            unsigned int aabb = 0;
            for (aabb = scene->aabbs_count - additional_aabbs_count; aabb < scene->aabbs_count; aabb++)
            {
                scene->aabbs[aabb].min.x = parse_float(i, t, buffer);
                (*i)++;
                scene->aabbs[aabb].min.y = parse_float(i, t, buffer);
                (*i)++;
                scene->aabbs[aabb].min.z = parse_float(i, t, buffer);
                (*i)++;
            }
        }
    }
    if ((((*t)[*i].type == JSMN_STRING) && (jsoneq(buffer, &(*t)[*i], "MAX") == 0)) ||
        (((*t)[*i].type == JSMN_STRING) && (jsoneq(buffer, &(*t)[*i], "max") == 0)))
    {
        (*i)++; // consume the string "min"
        if ((*t)[*i].type == JSMN_ARRAY)
        {
            (*i)++;//consume the array [
            unsigned int aabb = 0;
            for (aabb = scene->aabbs_count - additional_aabbs_count; aabb < scene->aabbs_count; aabb++)
            {
                scene->aabbs[aabb].max.x = parse_float(i, t, buffer);
                (*i)++;
                scene->aabbs[aabb].max.y = parse_float(i, t, buffer);
                (*i)++;
                scene->aabbs[aabb].max.z = parse_float(i, t, buffer);
                (*i)++;
            }
        }
    }
}

void parse_pair(int* i, jsmntok_t** const t, char* const buffer, Scene* scene)
{
    if (((*t)[*i].type == JSMN_STRING) && (jsoneq(buffer, &(*t)[*i], "pair") == 0))
    {
        (*i)++; // consume the string "min"
        if ((*t)[*i].type == JSMN_ARRAY)
        {
            int array_size = (*t)[*i].size;
            (*i)++; // consume the string "min"
            if ((*t)[*i].type == JSMN_OBJECT)
            {
                int l = 0;
                for (l = 0; l < array_size; l++)
                {
                    (*i)++;
                    if (((*t)[*i].type == JSMN_STRING) && (jsoneq(buffer, &(*t)[*i], "type") == 0))
                    {
                        (*i)++; // consume the string "type"
                        if (jsoneq(buffer, &(*t)[*i], "sphere") == 0)
                        {
                            (*i)++; // consume the string "sphere"
                            parse_sphere(i, t, buffer, scene);
                        }
                        else if (jsoneq(buffer, &(*t)[*i], "AABB") == 0)
                        {
                            (*i)++; // consume the string "AABB"
                            parse_aabb(i, t, buffer, scene);
                        }
                    }
                }
            }
        }
    }
}

void parse_csg(int* i, jsmntok_t** const t, char* const buffer, Scene* scene)
{
    if (((*t)[*i].type == JSMN_STRING) && (jsoneq(buffer, &(*t)[*i], "op") == 0))
    {
        (*i)++;
        if (((*t)[*i].type == JSMN_STRING) && (jsoneq(buffer, &(*t)[*i], "difference") == 0))
        {
            (*i)++;
            parse_pair(i, t, buffer, scene);
        }
        else if (((*t)[*i].type == JSMN_STRING) && (jsoneq(buffer, &(*t)[*i], "union") == 0))
        {
            (*i)++;
            parse_pair(i, t, buffer, scene);
        }
        else if (((*t)[*i].type == JSMN_STRING) && (jsoneq(buffer, &(*t)[*i], "intersection") == 0))
        {
            (*i)++;
            parse_pair(i, t, buffer, scene);
        }
    }
}

void parse_scene(char* filename, Scene* scene)
{
    long lSize;
    char * buffer;
    size_t result;

    FILE * pFile = fopen(filename, "rb");

    if (pFile == NULL) { fputs("File error", stderr); exit(1); }

    // obtain file size:
    fseek(pFile, 0, SEEK_END);
    lSize = ftell(pFile);
    rewind(pFile);

    // allocate memory to contain the whole file:
    buffer = (char*)malloc(sizeof(char)*lSize + 1);
    buffer[lSize] = '\0';
    if (buffer == NULL) { fputs("Memory error", stderr); exit(2); }

    // copy the file into the buffer:
    result = fread(buffer, 1, lSize, pFile);
    if (result != lSize) { fputs("Reading error", stderr); exit(3); }

    jsmn_parser parser;
    unsigned int number_of_tokens = 65536;
    jsmntok_t* t = (jsmntok_t*)malloc(sizeof(jsmntok_t) * number_of_tokens); //freed at the bottom.

    int i = 0;
    int j;

    int size;

    jsmn_init(&parser);
    int r = jsmn_parse(&parser, buffer, strlen(buffer), t, number_of_tokens);

    i++; //consume the first object
    while (i < r)
    {
        if ((t[i].type == JSMN_STRING) && (jsoneq(buffer, &t[i], "main_camera") == 0))
        {
            i++; //consume the camera string.
            if (t[i].type == JSMN_OBJECT)
            {
                parse_camera(&i, &t, buffer, &(scene->main_camera));
            }
        }
        else if ((t[i].type == JSMN_STRING) && (jsoneq(buffer, &t[i], "lenses_camera") == 0))
        {
            i++; //consume the camera string.
            if (t[i].type == JSMN_OBJECT)
            {
                parse_camera(&i, &t, buffer, &(scene->lenses_camera));
            }
        }
        else if ((t[i].type == JSMN_STRING) && (jsoneq(buffer, &t[i], "scene") == 0))
        {
            i++; //consume the scene string.
            if (t[i].type == JSMN_OBJECT)
            {
                int scene_item = 0;
                int scene_item_count = t[i].size;
                i++; //consume the object.
                for (int scene_item = 0; scene_item < scene_item_count; scene_item++)
                {
                    if ((t[i].type == JSMN_STRING) && (jsoneq(buffer, &t[i], "triangles") == 0))
                    {
                        i++; // consume the string "quadruples"
                        if (t[i].type == JSMN_ARRAY)
                        {
                            scene->triangles_count = t[i].size;
                            scene->triangles = (Triangle*)malloc(sizeof(Triangle) * scene->triangles_count);
                            int triangle = 0;

                            size = t[i].size;
                            i++;//consume the object and get to the string position/color/normal
                            for (triangle = 0; triangle < size; triangle++)
                            {
                                if (t[i].type == JSMN_OBJECT)
                                {
                                    int attribute = 0;
                                    int attribute_count = t[i].size;
                                    i++; //consume the object.
                                    for (attribute = 0; attribute < attribute_count; attribute++)
                                    {
                                        if (jsoneq(buffer, &t[i], "vertices") == 0)
                                        {
                                            i++; // consume the string "vertices"
                                            if (t[i].type == JSMN_ARRAY)
                                            {
                                                i++;
                                                scene->triangles[triangle].v0.x = parse_float(&i, &t, buffer);
                                                i++;
                                                scene->triangles[triangle].v0.y = parse_float(&i, &t, buffer);
                                                i++;
                                                scene->triangles[triangle].v0.z = parse_float(&i, &t, buffer);
                                                i++;
                                                scene->triangles[triangle].v1.x = parse_float(&i, &t, buffer);
                                                i++;
                                                scene->triangles[triangle].v1.y = parse_float(&i, &t, buffer);
                                                i++;
                                                scene->triangles[triangle].v1.z = parse_float(&i, &t, buffer);
                                                i++;
                                                scene->triangles[triangle].v2.x = parse_float(&i, &t, buffer);
                                                i++;
                                                scene->triangles[triangle].v2.y = parse_float(&i, &t, buffer);
                                                i++;
                                                scene->triangles[triangle].v2.z = parse_float(&i, &t, buffer);
                                            }
                                            i++;
                                        }
                                        else if (jsoneq(buffer, &t[i], "name") == 0)
                                        {
                                            i++; // consume the string "name"
                                            i++; // consume the name
                                        }
                                        else if (jsoneq(buffer, &t[i], "color") == 0)
                                        {
                                            i++; //consume the string color
                                            //scene->triangles[triangle].color_index = (GLuint)parse_float(&i, t, buffer);
                                            i++; // consume the int
                                        }
                                        /*else if (jsoneq(buffer, &t[i], "power") == 0)
                                        {
                                            i++; //consume the string power
                                            //only grabbing the first float of the vec4
                                            scene->triangles[triangle].power = parse_vec4(&i, t, buffer).x;
                                        }*/
                                        else
                                        {
                                            i++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else if ((t[i].type == JSMN_STRING) && (jsoneq(buffer, &t[i], "lenses") == 0))
                    {
                        i++; // consume the string "quadruples"
                        if (t[i].type == JSMN_ARRAY)
                        {
                            scene->lenses_count = t[i].size;
                            scene->lenses = (Lens*)malloc(sizeof(Lens) * scene->lenses_count);
                            int lens = 0;

                            size = t[i].size;
                            i++;//consume the object and get to the string position/color/normal
                            for (lens = 0; lens < size; lens++)
                            {
                                if (t[i].type == JSMN_OBJECT)
                                {
                                    int attribute = 0;
                                    int attribute_count = t[i].size;
                                    i++; //consume the object.
                                    for (attribute = 0; attribute < attribute_count; attribute++)
                                    {
                                        if (jsoneq(buffer, &t[i], "name") == 0)
                                        {
                                            i++; // consume the string "vertices"
                                            i++;
                                        }
                                        else if (jsoneq(buffer, &t[i], "cylinder_radius") == 0)
                                        {
                                            i++; // consume the string "vertices"
                                            scene->lenses[lens].cylinder_radius = parse_float(&i, &t, buffer);
                                            i++;
                                        }
                                        else if (jsoneq(buffer, &t[i], "cylinder_z1") == 0)
                                        {
                                            i++; // consume the string "vertices"
                                            scene->lenses[lens].cylinder_z1 = parse_float(&i, &t, buffer);
                                            i++;
                                        }
                                        else if (jsoneq(buffer, &t[i], "cylinder_z2") == 0)
                                        {
                                            i++; // consume the string "vertices"
                                            scene->lenses[lens].cylinder_z2 = parse_float(&i, &t, buffer);
                                            i++;
                                        }
                                        else if (jsoneq(buffer, &t[i], "radius1") == 0)
                                        {
                                            i++; // consume the string "vertices"
                                            scene->lenses[lens].radius1 = parse_float(&i, &t, buffer);
                                            i++;
                                        }
                                        else if (jsoneq(buffer, &t[i], "radius2") == 0)
                                        {
                                            i++; // consume the string "vertices"
                                            scene->lenses[lens].radius2 = parse_float(&i, &t, buffer);
                                            i++;
                                        }
                                        else if (jsoneq(buffer, &t[i], "center_z1") == 0)
                                        {
                                            i++; // consume the string "vertices"
                                            scene->lenses[lens].center_z1 = parse_float(&i, &t, buffer);
                                            i++;
                                        }
                                        else if (jsoneq(buffer, &t[i], "center_z2") == 0)
                                        {
                                            i++; // consume the string "vertices"
                                            scene->lenses[lens].center_z2 = parse_float(&i, &t, buffer);
                                            i++;
                                        }
                                        else if (jsoneq(buffer, &t[i], "refractive_index") == 0)
                                        {
                                            i++; // consume the string "vertices"
                                            scene->lenses[lens].refractive_index = parse_float(&i, &t, buffer);
                                            i++;
                                        }
                                        else
                                        {
                                            i++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    free(t);
}

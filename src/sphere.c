#include "sphere.h"

void init_sphere(Sphere *sphere){
    sphere->position.x = 0.0f;
    sphere->position.y = 0.0f;
    sphere->position.z = 0.0f;
    sphere->position.w = 1.0f;
    sphere->radius = 1.0;
    sphere->padding1 = 0.0;
    sphere->padding2 = 0.0;
    sphere->padding3 = 0.0;
}
